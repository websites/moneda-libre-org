# Site [monnaie-libre.fr](https://monnaie-libre.fr)

Ceci est le dépôt du site.

## Sommaire

[[_TOC_]]

<div class="panel panel-warning">
<div class="panel-body">
Si vous n'êtes pas développeur et souhaitez contribuer à la rédaction du site, passez par l'[administration du site](https://monnaie-libre.fr/admin/).
</div>
</div>

### Build Setup

```bash
## install dependencies
$ yarn install

## serve with hot reload at localhost:3000
$ yarn dev

## generate static project
$ yarn generate

## launch localy
$ yarn start
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

# Administration : le back office

Vous pouvez accéder à l'administration ici : https://monnaie-libre.fr/admin/
Le back office est conçu avec [netlify cms](https://www.netlifycms.org/) qui est lui même couplé à ce dépôt gitlab. Tout changement effectué en back office créera une demande de fusion dans ce dépôt.
Vous devez avoir un compte sur ce gitlab (git.duniter.org) pour pouvoir accéder au back office.

## Lancer le back office (netlify)

Si vous avez cloner le dépôt sur votre ordinateur, vous pouvez lancer le back office en local :

     yarn admin

Puis aller sur http://localhost:3000/admin/

## Configuration de netlify

- `static/admin/config.yml` Paramètres de netlify
- `static/admin/index.html` HTML de la page d'administration de netlify

# Design

## Illustration

Toutes les images utilisées sur le site :
https://www.figma.com/file/Gof5rulCCUMvwk3WDKlgVY/monnaie-libre.fr

Ressources :
https://www.pixeltrue.com/free-packs/minimalistic-illustrations#preview

## Afficher toutes les `class` de tailwindcss

     http://localhost:3000/_tailwind/

Sur cette page, vous pouvez copier une class css en cliquant sur le nom, ou maj+click pour copier plusieurs class.

## Icônes

https://fontawesome.com/icons?m=free
https://www.figma.com/file/Gof5rulCCUMvwk3WDKlgVY/H%C3%A9ro-monnaie-libre.fr?node-id=30%3A0

# Docs

- @nuxt/content https://content.nuxtjs.org/fr et https://nuxtjs.org/docs/2.x/directory-structure/content/
- @nuxt/svg https://github.com/nuxt-community/svg-module
- @nuxt/composition-api https://composition-api.nuxtjs.org/

Voir aussi`nuxt.config.js` pour la doc d'autres modules...

## Tailwindcss et Vue

https://tailwindcss.com/
https://play.tailwindcss.com/

### Templates et ressources

https://vuetailwind.com/
https://tailblocks.cc/
https://tailwindcomponents.com/
https://www.tailwindtoolbox.com/
https://mertjf.github.io/tailblocks/
http://blocks.wickedtemplates.com/
https://www.creative-tim.com/learning-lab/tailwind-starter-kit/documentation/download

## Divers

https://maizzle.com
https://vuetensils.stegosource.com/introduction.html

# Generate fake data

Modifier le fichier `test/generate-md-files.js` puis :

     node test/generate-md-files.js

Aide faker.js: https://cdn.rawgit.com/Marak/faker.js/master/examples/browser/index.html

# Docker

**Run the nuxt build site in a docker environment**

If you don't got it, run the following command to get the Nginx Docker image:

    docker pull nginx

This downloads all the necessary components for the container. Docker will cache these, so when we run the container we don’t need to download the nginx image(s) each time.

Then start our Nginx Docker container with this command:

    dk run --rm -d -v $(pwd)/dist:/usr/share/nginx/html -p 80:80 nginx:alpine

Go to http://localhost/ !

You can change the port. In the command above, replace `80:80` by `4000:80` and go to http://localhost:4000/

# Gitlab

## Graphql

Graphql Endpoint : https://git.duniter.org/api/graphql
Graphql Explorer : https://git.duniter.org/-/graphql-explorer

## About CI

https://blog.logrocket.com/how-to-auto-deploy-a-vue-application-using-gitlab-ci-cd-on-ubuntu/

# Discourse

## API Docs

https://docs.discourse.org/
