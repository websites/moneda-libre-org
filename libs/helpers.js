/**
 * Debounce function call
 *
 * @param {Function} fn - The function to debounce
 * @param {*} delay - Delay to wait before execute `fn`
 */
export function debounce(fn, delay) {
  let timeoutID = null
  return function () {
    clearTimeout(timeoutID)
    const args = arguments
    const that = this
    timeoutID = setTimeout(function () {
      fn.apply(that, args)
    }, delay)
  }
}

/**
 * Highlight words in a string by wrapping them with <mark></mark>.
 *
 * @param {String} query - The word to find and wrap in results
 * @param {Array} results - An array of objects coming from `document` where `title` and `description` will be highlighted
 *
 * @return {array} An array of object
 */
export function highlight(query, results) {
  const wrap = (content) => {
    return content.replace(
      new RegExp(query.trim(), 'gi'),
      (match) => `<mark>${match}</mark>`
    )
  }
  return results.map((item) => ({
    ...item,
    title: item.title && wrap(item.title),
    description: item.description && wrap(item.description),
  }))
}

/**
 * Filter results from nuxt `$content` because I don't know how to query all content and exclude some routes. So I remove routes from results.
 *
 * @param {Array} results - An array of objects coming from `document`
 *
 * @return {Array} Reduced results
 */
export function reduceResults(results) {
  return results.filter(
    (page) =>
      !page.path.startsWith('/ui') &&
      !page.path.startsWith('/ressources') &&
      page.title
  )
}

/**
 * Parse `document` to return meta for SEO.
 *
 * @return {Object} An object with meta good for SEO :)
 */
export function headDocument() {
  const title = this.document.title || this.$config.site_title
  const content = this.document.description || this.$config.site_description
  const image =
    this.document.image || `${this.$config.site_url}/img/og-home.jpeg`

  return {
    title,
    meta: [
      {
        hid: 'description',
        name: 'description',
        content,
      },
      // Open Graph
      { hid: 'og:title', property: 'og:title', content: title },
      {
        hid: 'og:description',
        property: 'og:description',
        content,
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: image,
      },
      // Twitter Card
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: title,
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content,
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: image,
      },
    ],
  }
}
