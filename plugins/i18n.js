import fr from '../locales/fr'
import en from '../locales/en'
import es from '../locales/es'

export default {
  defaultLocale: 'es',
  locales: [
    {
      code: 'en',
      name: 'English',
    },
    {
      code: 'es',
      name: 'Español',
    },
    {
      code: 'fr',
      name: 'Français',
    },
  ],
  strategy: 'no_prefix',
  vueI18n: {
    locale: 'es',
    fallbackLocale: 'es',
    messages: {
      es,
      en,
      fr,
    },
    dateTimeFormats: {
      en: {
        short: {
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
        },
        long: {
          year: 'numeric',
          month: 'short',
          day: 'numeric',
          weekday: 'short',
          hour: 'numeric',
          minute: 'numeric',
        },
      },
      es: {
        short: {
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
        },
        long: {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          weekday: 'short',
          hour: 'numeric',
          minute: 'numeric',
        },
        full: {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          weekday: 'long',
          hour: 'numeric',
          literal: 'h',
          minute: 'numeric',
        },
      },
      fr: {
        short: {
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
        },
        long: {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          weekday: 'short',
          hour: 'numeric',
          minute: 'numeric',
        },
        full: {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          weekday: 'long',
          hour: 'numeric',
          literal: 'h',
          minute: 'numeric',
        },
      },
    },
  },
}
