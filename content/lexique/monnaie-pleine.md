---
title: Moneda en plenitud
description: "Cuando el número de co-creadores sea estable."
synonyms:
  - moneda plena
---
En el  mundo de la moneda libre, lo que llamamos moneda en plenitud es el momento en que la media en número de DU por miembro sea estable.
    
Cuando el número de co-creadores ya casi no se mueve (N estable) la reevaluación del DUğ1 está muy muy cercana al 4,88% por semestre.   
Entonces cada seis meses el número de DUğ1 existentes se divide por 1,0488.

Después de alrededor de 40 años de existencia, una cuenta co-creadora sin intercambios llega a 3925 DUğ1 aproximadamente al final de semestre.

La aplicación de la re-evaluación (división por 1,0488) lleva la cuenta a tener alrededor de 3742 DUğ1  

En un semestre un co-creador crea 182 o 183 DUğ1. 3742 + 183 = 3925 DUğ1

A lo largo del semestre la cantidad de DUğ1 creados en tota la vida puede variar entre 3742 y 3925 por cada co-creador de moneda. Es imposible crear más DUğ1. 

Así que la moneda en plenitud ocurre cuando el número de miembros es estable desde al menos 40 años.