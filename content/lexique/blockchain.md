---
title: Blockchain
description: Forma de almacenamiento y transmisión de datos sin intermediarios...
synonyms:
  - cadena de bloques
---
Forma de almacenamiento y transmisión de datos sin intermediarios en forma de bloques ligados los unos a los otros y protegidos contra cualquier modificación.