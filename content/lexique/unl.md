---
title: UNL
description: Unidades No Libres (monedas no libres)
synonyms:
  - MNL
---

## UNL o MNL

Unidad No Libre o Moneda No Libre designa toda moneda que no respeta las [4 libertades económicas](/comprender#4-libertés), axiomas definidos en la <lexique title="trm">Teoría Relativa de La moneda.</lexique>.

Designa generalmente todas las monedas <i>fiat</i> (monedas deuda emitidas por los estados) y todas las monedas que no están creadas a partes iguales entre todos los usuarios(Bitcoin, Ethereum, Monedas Locales, etc.)

Ejemplo:
El término <b>UNLE</b>: Unidad No Libre Europea se usa para designar la moneda €uro.
