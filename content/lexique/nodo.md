---
title: Nodo
description: Un nodo es un servicio informático que hace funcionar la moneda 
synonyms:
  - nodos
---

Los nodos son ordenadores dotados del software Duniter que gestiona la <lexique>blockchain</lexique> de la Ğ1.
Estos ordenadores están puestos a disposición por usuarios de la Ğ1.
Son pequeños micro-ordenadores que consumen muy poco o incluso a veces alojados en servidores de Internet (conocidos como VPS).

Cada nodo Duniter verifica que el contenido de cada bloque respeta todas las reglas de la Ğ1. El cálculo de un bloque está concebido para durar de media 5 minutos. 

Sólo los nodos con la llave privada de un co-creador o miembro de la <lexique title="Red de Confianza">red de confianza</lexique>, pueden forjar un nuevo bloque en la blockchain. Son los forjadores (y no minadores).

Los nodos con una llave privada que no forma parte de la red de confianza no pueden inscribir en la blockchain, son útiles para comunicar con los programas <lexique>cliente</lexique>(ej. Cesium). Se conocen como nodos "espejos". 

Actualmente el cliente Cesium obliga a configurar en ajustes un nodo Duniter. Lo mejor es no elegir el mismo nodo que el de tus vecinos para repartir la carga de trabajo. 

Los nodos también pueden estar alojados en redes en malla alternativas al Internet (ejemplo <b>#Guifinet</b> en España) o <b>#FFDN</b> en todo el mundo (Ver [mapa](https://db.ffdn.org/)).