---
title: Junistas
description: Los/as usuario/as de la juna
synonyms:
  - junista
  - luniste
  - lunistas
---

## Un Junista (en singular).

Designa un humano/a que usa la Ğ1.
Ya sea simple usuario/a o miembro de la <lexique title="RdC">red de confianza</lexique>.

## Un moneda-librista

Del francés <i>monnaie-libriste</i> designa todo usuario de una moneda libre en el sentido de la <lexique>TRM</lexique>. A día de hoy sólo existe una moneda libre, la Ğ1.
La distinción con un Junista se verá más claramente conforme vayan apareciendo otras monedas libres.