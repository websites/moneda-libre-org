---
title: Red de Confianza
description: Red de Confianza (WOT o Web Of Trust)
synonyms:
  - rdc
  - RdC
---
## Sistema de identificación descentralizado

La *Red de Confianza* (o de Reconocidos) tiene como objetivo la identificación de todos los humanos co-creadores de moneda.
Sólo un ser humano vivo puede ser co-creador de moneda, por eso hay que identificar a cada humano.
Un ser humano sólo puede ser co-creador de la moneda si está reconocido y certificado por al menos otros cinco co-creadores de moneda, al mismo tiempo que respeta la <lexique title="Regla de la distancia">regla de los cinco pasos</lexique>.
Previamente debe adherir su candidatura, transformando uno de sus monederos en *"cuenta miembro"*. 


## Certificación

La identificación se hace mediante la certificación.
Cuando un co-creador de moneda es capaz de identificar a un humano como único y vivo, puede certificarle.
Esto implica conocer bien al/a la <lexique>junista</lexique>, ser capaz de reconocerlo/a y de contactarlo/a fuera de la red de la Ğ1. 
Esta certificación es vigente durante sólo dos años (730 días y 12 horas), una certificación se puede renovar tantas veces como se quiera, o no. 

## Reserva de certificaciones

Cada co-creador puede tener un máximo de 100 certificaciones emitidas en curso de válidez. Cada certificación tiene una vigencia de 2 años. \
Cualquier certificación emitida desde hace más de dos años ya no está contabilizada entre las 100. \
Entonces cada co-creador puede emitir 100 certificaciones en 2 años consecutivos.

## Candidatura / Adhésion

La adhesión se hace en el momento de la transformación de un monedero simple en cuenta miembro. 
Para poder seguir siendo co-creador de moneda, un co-creador tiene que renovar su adhesión de cuenta miembro en un plazo de 365 días y 6 horas a partir de la fecha de su última renovación. 

## Más detalles

Ver la [red de confianza en detalle](https://foro.moneda-libre.org/t/como-funciona-la-red-de-confianza/1920)