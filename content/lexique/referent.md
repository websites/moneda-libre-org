---
title: Referentes
description: Los co-creadores más activos tejiendo la red de confianza
synonyms:
  - tejedores
  - tejedor
  - referente
---

Los co-creadores referentes(o tejedores) son los humanos más activos en la <lexique title="RdC">red de confianza</lexique>, es decir los que han recibido **Y** emitido cierto número de certificaciones. 

Un miembro de la <lexique>RdC</lexique> Ğ1 es miembro referente cuando ha recibido y emitido al menos Y[N] certificaciones en las cuales N es el número de miembros de la RdC y Y[N] = techo (N<sup>(1/5)</sup>). 

Dicho de otra forma el redondeo al entero superior de la raíz de la quinta parte sobre el número de miembros. 
Ejemplos: 
    • Entre 1.024 < N ≤ 3.125 miembros tenemos que Y[N] = 5
    • Entre 7.776 < N ≤ 16.807 miembros tenemos que Y[N] = 7
    • Entre 59.049 < N ≤ 100.000  miembros tenemos que Y[N] = 10

Porque

- 5<sup>5</sup> = 3.125
- <b>6<sup>5</sup> = 7.776</b>
- 7<sup>5</sup> = 16.807
- 9<sup>5</sup> = 59.049
- etc.


*Actualmente (14 de Diciembre 2022) un miembro es referente si ha recibido al menos 6 certificaciones **y** emitido al menos 6 certificaciones.*

La <lexique>regla de la distancia</lexique> se calcula únicamente a partir de estos miembros referentes(o tejedores)

Cada vez que sube un dígito (ej. de 5 a 6) el número mínimo de certificaciones necesarias para ser referente o tejedor, durante pocas semanas, es más fácil entrar en la red de confianza por la <lexique>regla de distancia</lexique> ya que la cantidad de miembros que ahora son referentes en la red de confianza disminuye de golpe.