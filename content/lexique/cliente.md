---
title: Cliente
description: Programa que conecta con la red Ğ1
---
## Para los usuarios de la Ğ1
Para usar la Ğ1 se necesita un programa ejectuable en un ordenador o móvil.

Algunos de los programas clientes de la Ğ1 :
* [Cesium](https://cesium.app/es/) El más usado, funciona en casi todas las plataformas.
* Sakia, un poco más complejo, a futuro remplazado por Tikka.
* Silkaj (python) para usar en la línea de comando.
* Gecko todavía no está disponible pero será el monedero por defecto para Duniter-v2.
* Tikka que remplazará a futuro Sakia

## Los clientes se conectan a los nodos.
Los clientes se conectan automáticamente a un <lexique>nodo</lexique> Duniter. Actualmente el cliente Cesium propone al usuario seleccionar manualmente un nodo en los ajustes.
