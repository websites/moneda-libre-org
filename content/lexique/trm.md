---
title: Teoría Relativa de la Moneda
description: Teoría monetaria usada como fundamento de la moneda libre
synonyms:
  - trm
---
## Una Teoría y un libro

La teoría se describe por Stéohane Laborde en su libro *"la Théorie Relative de la Monnaie (se suele usar sus siglas TRM)"*.

### La teoría

Se basa en 4 axiomas: las [4 libertades económicas](/comprender#4-libertés).
Para respetar estas 4 libertades, la teoría describe con un cálculo matemático la creación monetaria bajo forma de un dividendo universal como: **DU=c(M/N)**.\
El Dividendo Universal 'DU' es una parte 'c' de la masa monetaria por indivíduo 'M/N'. 

### El libro

El libro está disponible en (francés)[http://trm.creationmonetaire.info/] e (inglés)[https://en.trm.creationmonetaire.info/] de forma libre y gratuita.
También se puede comprar una copia física del libro original...

En castellano, existe una versión traducida de un resumen del libro conocido como la [TRM en detalle](https://foro.moneda-libre.org/t/teoria-relativa-de-la-moneda-trm-en-detalle/95)... y en 2023 saldrá a la luz la versión traducida de la TRM original, podéis agradecer a los traductores a través de la llave de junas del equipo de traducción: <small>AwAXgRQQVRaXNCUjyaZ7S2YwHVsgg9gkfiYmeDZXng4Z</small>