---
title: Regla de la distancia
description: Todos estamos conectados.
synonyms:
  - regla de distancia
---

## El mundo es un pañuelo.

**No es cuestión de geografía sino de vínculo.**

Alicia conoce a Beatriz, Beatriz está a un paso de Alicia. 
Beatriz conoce a María, María está a un paso de Beatriz. 
María conoce a Diana. Diana está a un paso de María. 
Alicia no conoce ni a María ni a Diana, pero gracias a Beatriz, María está a 2 pasos de Alicia y Diana está a 3 pasos de Alicia. 

Una teoría propuso que todos estamos conectados a menos de [seis grados de separación](https://es.wikipedia.org/wiki/Seis_grados_de_separaci%C3%B3n) de cualquier persona en el mundo. 

## Red de confianza.

En principio todos estamos poco alejados los unos de los otros gracias a los lazos que tenemos entre gente cercana. La red de confianza no hace nada más que registrar en la <lexique>blockchain</lexique> los lazos tejidos entre los seres humanos que se conocen. 

Una cuenta co-creadora demasiado alejada de los demás sería la de alguien que no teje suficientes lazos o más probablemente un ser imaginario (una cuenta falsa). 
De ahí esa regla de los 5 pasos o de la distancia. 

Sólo se contabiliza a los miembros <lexique>referentes</lexique> (los miembros que han tejido suficientes lazos), y permite que hasta un 20% de estos miembros esté a más de 5 pasos. **Ojo, los pasos sólo se cuentan en un sentido.**

Aline certifica a Barbara. Barbara se encuentra a un paso de Aline.

Pero si Aline no está certificada por Barbara no está a un paso de Barbara. Barbara tiene que certificar a Alina para que funcione en el otro sentido.

Entonces es inútil certificar a referentes para acarcarse a ellos, hay que acercarse a ellos para que ellos te certifiquen. 


## Resumen

Para ser y permanecer co-creador de moneda, hay que estar a menos de 5 pasos del 80% de los co-creadores <lexique title="referente">referentes</lexique>. 

El porcentaje de miembros referentes alcanzables se llama *calidad* (a 4 pasos o menos) o *distancia* (a 5 pasos o menos). Más info [aquí](/faq/que-es-la-calidad-de-miembro-o-de-dosier).