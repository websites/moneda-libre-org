---
title: DU
description: 'Dividendo Universal y unidad de medida'
synonyms:
  - dividendo universal
  - dividendo
---

## Dividendo Universal

Cada co-creador de moneda libre crea una parte de moneda proporcional a la masa monetaria dividida por el número de miembros.
**DU = c×(M/N)**
Esta creación monetaria se llama Divivendo Universal o DU.
Este DU es lo que define que una moneda es libre en el sentido de la <lexique>TRM<lexique>. <i>(De la misma forma que se llama software libre a aquél que cumple con las 4 libertades)</i>

## Unidad de medida

En la moneda libre Ğ1 este Dividendo Universal se crea cada día en cada cuenta co-creadora de moneda. (Una sóla cuenta co-creadora por cada ser humano vivo)
La fórmula se deriva de esta forma:
**DU<sub>(t+1)</sub>=DU<sub>(t)</sub> + c<sup>2</sup>×(M/N)<sub>(t)</sub> **. Este DUğ1 se reevalua cada 6 meses (el primer día de cada equinoccio)

Este DUğ1 es un invariante espacio-temporal: un ser humano crea 1 DUğ1 al día sin importar el lugar ni la época, y este DUğ1 representa siempre la misma parte de la masa monetaria global.

Al usar este DUğ1 diario como unidad de medida, la evolución de los precios no depende de la masa monetaria sino de factores externos a la moneda: oferta/demanda, crisis, escasez de recursos y otros imponderables. 