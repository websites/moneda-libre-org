---
title: La moneda libre y la democracia
description: ¿ Qué puede aportar la moneda libre a la democracia ?
---
## DEMOS (el pueblo) + KRATOS (el poder)

Sabemos que la principal fuente de poder viene del dinero. Cuanto más dinero se posee más poder se tiene.\
También sabemos que el tiempo de presencia en los medios, influye directamente en los resultados electorales. Los medios son controlados por las grandes fortunas, o sea que quienes más dinero tienen son quienes deciden el resultado de las elecciones.

*Gráfica con la correlación del tiempo de presencia mediática (en rojo) en las elecciones francesas de 2012, por cada político y sus resultados (en azul)*:
![Gráfica de las elecciones francesas](http://etienne.chouard.free.fr/Europe/Correlation_heures_TV_resultats_election.jpg)


## Retomar el poder

Con la moneda libre la decisión de creación o inyección de moneda en la economía, para financiar una u otra cosa, deja de ser un privilegio de los banqueros o de los ministros (vía la creación monetaria actual realizada mediante los créditos bancarios). 

En la moneda libre esta se realiza por cada invididuo a través del <lexique>DU</lexique> que se crea cada día (sin deuda, ni contra-partida de cualquier naturaleza) en sus cuentas cocreadoras, y que ellos mismos inyectan directamente a la economía cuando utilizan o intercambian sus Ğ1 (junas) recién creadas. A través de esos intercambios muestran sus valores e influyen directamente en el tipo de mundo y sociedad nueva que quieren construir.


## Igualdad

Con la creación monetaria de igualdad para todos, y la convergencia de los saldos de las cuentas hacia la media, el poder ya no está en mano de unos, sino en mano de todos.\
Cualquiera tiene el mismo peso en el sistema.