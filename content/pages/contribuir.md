---
title: Contribuir
description: ¡Todas las infos para participar!
plan: ui/main-flatplan
---
## Cambian el mundo

A veces se escucha a algunos <lexique>junistas</lexique> decir que la Ğ1 es "*una obra de arte colectiva*".

Y es verdad que hay mucha gente que hace cosas alrededor de las monedas libres en general, o de la Ğ1 en particular.

Es toda una sinfonía que se pone en marcha, sin que nadie sepa bien cómo.

Si la Ğ1 existe hoy es gracias a toda esta gente:

* Los que desarrollan las herramientas: encontrarás sus nombres en la página de [Duniter](https://duniter.org/), el motor de la blockchain. 

* Los que validan las transacciones de la blockchain [forjando bloques](https://duniter.org/fr/miner-des-blocs/).

* Quienes traducen el software.

* Quienes redactan la documentación.

* Quienes reportan errores o fallos. 

* Quienes desarrollan la economía mediante sus [intercambios](https://girala.net/), valoran la Ğ1 y demuestran a diario que una moneda libre permite intercambiar.

* Quienes hablan de ella a su entorno.

* Quienes crean grupos locales.

* Quienes organizan partidas de [Ğeconomicus](https://foro.moneda-libre.org/t/geconomicus-iberico/669).

* Quienes crean memes graciosos.

* Quienes dan charlas para explicar la creación monetaria.

* Quienes crean contenido pedagógico.

* Quienes escriben libros.

* Quienes hacen creaciones gráficas.

* Quienes ponen a disposición infraestructuras que permiten alojar contenido.

* Y a todos quienes hemos olvidado en esta lista.

## ¡Te toca a tí!

La Ğ1 no pertenece a nadie.

De hecho, si te han dicho que esta página era la "*página oficial*" de las monedas libres o de la Ğ1, aclarémonos: ni la moneda libre ni la Ğ1 pueden tener "*página oficial*", porque no hay autoridad que les gobierne. 

Quizás esta página suele ser la que aparece primero en los buscadores cuando tecleas "*moneda libre*", pero otras páginas hablan de las monedas libres o de la Ğ1 y no tienen menos legitimidad que ésta para hacerlo. 

Como pasó en los "chalecos amarillos" de Francia, algunas personas pueden intentar registrar "moneda libre", pero se darán cuenta que "moneda libre" no es más que es la asociación de dos conceptos comunes.

La Ğ1 no tiene cabeza.

La Ğ1 no tiene centro.

Así que eres libre de contribuir como quieras, sin esperar a que nadie te de permiso para hacerlo.

Si quieres colaborar con otra gente en algunos proyectos, puedes avisar en cualquier plataforma o red en la cual se encuentran junistas: en general, los junistas sabrán cómo ponerte en relación con otros junistas para que podaís juntar vuestros esfuerzos. 😉