---
title: GFM
description: Test Github Flavored Markdown
---

# GFM

## Breaks newline

This is a paragraph...
and a new paragraph

## Autolink literals

www.example.com, https://example.com, and contact@example.com.

## Strikethrough

~one~ or ~~two~~ tildes.

## Table

| a   | b   |   c |  d  |
| --- | :-- | --: | :-: |
| 1   | 2   |   3 |  4  |

## Tasklist

- [ ] to do
- [x] done
