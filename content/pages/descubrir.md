---
title: Descubrir
description: Informaciones para poder entender
plan: ui/main-flatplan

---
## La moneda no es el fruto del trabajo.

La moneda que tienes no es el fruto de tu trabajo.
La moneda es lo que aceptas **como intercambio** de los frutos de tu trabajo.

## ¿Pero entonces, quién produce la moneda?

En el sistema monetario regido por los bancos, son los bancos comerciales quienes crean el dinero que utilizas a diario.
El propio gobierno te lo dice: <https://www.economie.gouv.fr/facileco/creation-monetaire-definition>
Es el banco el que decide quién tiene derecho al dinero, según sus criterios (generalmente el máximo beneficio).
Por supuesto, esta creación no es gratuita, hay que pagar intereses.
En general, hay que devolver más dinero del que hay en circulación.

Consecuencia: Carrera por los beneficios, competencia, quiebra para los más frágiles. Esta carrera por los beneficios conduce, entre otras cosas, al agotamiento de los recursos y la biodiversidad.

## ¿Y la moneda libre?

En el sistema de la moneda libre, la moneda se crea por igual para todos los miembros, sin deudas ni intereses que devolver.
Cada miembro crea su propia porción de dinero, es como una especie de renta básica, no una renta de subsistencia.
La creación permanente de moneda, alivia el miedo al futuro y promueve un comportamiento de colaboración y apoyo mutuo.

## En vídeo

Explicación resumida Carmela(8min):

<iframe title="La Moneda Libre G1" width="560" height="315" src="https://tube.p2p.legal/videos/embed/c4ccdbc2-3b6c-4780-8206-6a7d6d511012" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

La moneda libre sin tapujos por Corinne (18min) :

<iframe title="La Moneda Libre (sin tapujos)" width="560" height="315" src="https://tube.p2p.legal/videos/embed/c5d12195-9a30-4ae4-b4cd-430245a23616" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

- - -

## Una unidad de medida

Utilizamos la moneda como unidad de medida. Pero ¿cómo podemos aplicar esta unidad de medida si no representa el mismo valor en el espacio y en el tiempo?

El metro se ha definido como la distancia del Ecuador al polo dividida por 10 millones. El grado Celsius se define como la diferencia de temperatura entre la congelación y la evaporación del agua dividida por 100.

El segundo del Sistema Internacional se define por la duración de un número de oscilaciones (9.192.631.770 para ser exactos) relacionadas con un fenómeno físico en el que interviene el átomo de cesio.

Estas unidades de medida son invariables y universales para todos los seres humanos en el tiempo y el espacio.

Sin embargo, no tenemos ningún punto de referencia para definir el valor de una unidad monetaria. Este valor varía con el tiempo y el espacio. Una moneda puede devaluarse simplemente creando más unidades.

Con la moneda libre creamos una nueva unidad de medida: la cantidad de dinero producida cada día por cada individuo. Cuando la moneda libre esté plenamente implantada, esta cantidad representará siempre la misma porción de dinero en relación con la oferta monetaria total. Esto hace que sea invariante a través del tiempo y el espacio.

**El DUğ1 se convierte en una unidad de medida de valor universal.**
