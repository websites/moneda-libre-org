---
title: Comenzar
description: Por dónde comenzar a utilizar la Ğ1
plan: ui/main-flatplan
---
## Abrir una cuenta

<alert type="info">Comienza siempre eligiendo una *cuenta simple monedero*, **solo después** de haberte reencontrado con otros cocreadores podrás [convertirla a cuenta *miembro* cocreadora de moneda](/faq/como-puedo-ser-cocreador)</alert>

### 1. Descargar Cesium

En tu ordenador dirígete a <https://cesium.app/es/>
y en la página de "Descargar" elige la versión que te convenga.

Te aconsejamos que elijas la extensión correspondiente a tu navegador.

Si eliges una extensión, verás el logo de Cesium al final de la barra de direcciones.

En tu móvil ve a tu "Store" o Gestor de apps, busca la aplicación Cesium G1 e instálala.

### 2. Modificar los parámetros por defecto.

Hay nodos configurados por defecto, si todo el mundo se queda en estos <lexique>nodos</lexique> por defecto se sobrecargarán y será lento.

Consulta el procedimiento indicado en el [foro](https://foro.moneda-libre.org/t/nodos-cesium-vs-nodos-duniter/1910).

Comprueba que estás en un nodo actualizado.
Las próximas versiones de monederos lo harán automáticamente. Mientras tanto, hay que hacerlo a mano.

#### Elige la visualización en DUğ1.
Hay muchas discusiones sobre si contar en [ğ1 o en DUğ1](/faq/la-unidad-es-el-du-o-la-juna-g1).

La visualización en DUğ1 te abre a una nueva forma de pensar.
*Juega con los diferentes ajustes para ver los cambios que produce en la pantalla*

Descubre más información a través de las diferentes pestañas de Cesium. Por ejemplo la pestaña de "Moneda" te da estadísticas en tiempo real de la moneda, su masa monetaria, el promedio por cocreador, etc. La pestaña de "Red" te da información transparente sobre la blockchain.

### 3. Elegir una frase secreta/contraseña

La conexión a tu cuenta se hace mediante dos credenciales secretas: una **frase secreta** y una **contraseña**. Estas dos partes tienen que permanecer secretas y no comunicárselas a nadie. Para mayor seguridad es recomendable tener frases secretas bastante largas, de unos 25 carácteres o más.

<alert type="warning">No existe ningun sistema de recuperación de "contraseña olvidada", tienes que apuntar cuidadosamente tus contraseñas.</alert>

<alert type="info">También puedes usar un gestor de contraseñas</alert>

### 4. Abrir la cuenta

Una vez elegidas y memorizadas las contraseñas, puedes abrir tu cuenta.

Pulsar  el botón "crear una cuenta" no es indispensable. Si has guardado bien las contraseñas puedes conectarte directamente. Si pasas por este botón, elige "monedero simple".

Después de haber tecleado las contraseñas, verás aparecer tu llave pública. Se recomienda realizar varias veces la operación desconexión-reconexión para comprobar que siempre aparece la misma llave pública.

Cuando aparece una llave pública diferente es que has tecleado una errata.

<alert type="warning">El programa distingue mayúsculas, mínusculas, tildes o espacios, por lo tanto cualquier error de este tipo genera otra llave. Hay que ser muy preciso.</alert>

### 5. Completar tu perfil (o no)

Puedes mantener tu cuenta totalmente anónima.
Puedes poner un apellido, nombre o seudónimo para ayudar a tus compañeros a localizarte cuando quieran hacerte un pago. 
También puedes indicar tu dirección más o menos parcialmente para que las personas de tu región puedan encontrarte en el mapa.

Esto se hace en la página "Mi Cuenta", en el botón "Editar mi perfil". Siempre podrás modificar o suprimir este perfil.

<alert type="warning">Las informaciones registradas en tu perfil son visibles para todo el mundo, también para quienes no pertenecen a la moneda libre. Evita dejar rastros muy personales en Internet.</alert>

### 6. Activar las notificaciones por mail

Recuerda activar dentro de Cesium el servicio de notificación mail para recibir un mensaje si alguien intenta entrar en contacto contigo mediante césium, por ejemplo un vecino que te ha encontrado en el mapa o para ver un historial de tus transacciones.
Este servicio de notificaciones se hace en la parte "servicio en línea" en la página "mi cuenta" haciendo clic en "Agregar un servicio".
Ojo, no se verifica tu dirección mail, no te equivoques entrando los datos.

## Participar en los encuentros

No se necesita estar certificado para usar la moneda y hacer intercambios.
Ahora que has creado tu cuenta, ¡lo más importante es usarla!
Tienes que participar en los encuentros para hacer intercambios y hacer vivir esta nueva moneda. 

* Lee el [foro](https://foro.moneda-libre.org/). En la página del [calendario](https://foro.moneda-libre.org/calendar), encontrarás todos los eventos indicados en el foro. 
* Busca los que están cerca de ti. Puedes proponer tus propios eventos, invitar a junistas a visitarte. 
* Busca grupos en tu región. Ver el [mapa](https://carte.monnaie-libre.fr?members=false) de los eventos y grupos locales.
* Utiliza [ğirala](https://girala.net) y [ğchange](https://www.gchange.fr/#/app/home), para publicar tus anuncios de bienes y servicios.
* Acoge a junistas de vacaciones con [airbnjune](https://airbnjune.org/).

Al principio, tu cuenta está vacía, para "ganar" tus primeras junas tienes que empezar a vender. Vacía tus armarios de las cosas que no usas, propón tus propias creaciones artesanales o culinarias. También puedes ofrecer servicios.


## Vídeo:

### Creación de una cuenta moneda libre en Cesium

Vídeo tutorial sobre cómo crear una cuenta moneda libre en la aplicación Césium. Supone que ya te has bajado e instalado Cesium en tu ordenador, tablet o móvil via [https://cesium.app](https://cesium.app/)

<iframe title="Tutorial Cesium (I) - Crear cuenta monedero simple" width="560" height="315" src="https://tube.p2p.legal/videos/embed/18d1fccd-ad2c-40a3-a7c4-1ecf7fb1cfc6" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

### Más vídeos
Puedes encontrar muchos más vídeos en [nuestro canal de vídeos](https://tube.p2p.legal/my-library/video-playlists/6e6ac81e-888d-4101-9d2b-e5e55c813649) incensurable y libre peertube.

## ¡Ve despacio!

La Ğ1 es un proyecto reciente (ha nacido en el 2017)

Considera Ğ1 como un experimento de tamaño real. 

No pongas todas tus esperanzas en la Ğ1.

Es posible que encuentres algunas ventajas en usar la Ğ1, pero en ningún caso resolverá todos tus problemas. 

## ¿Tienes preguntas?

* Consulta la sección de [FAQ o Preguntas y Respuestas](/faq) de esta página.
* Busca otras respuestas en el [foro](https://foro.moneda-libre.org/)
* Pregunta en directo durante las videoconferencias o [formaciones](https://foro.moneda-libre.org/search?q=formacion%20G1)