---
title: maj-v2
description: Plan de Comunicación Duniter v2
---
# Plan de Comunicación

## ¿Cuándo se les informará sobre la progresión del proyecto?

1. **Octubre:** ¿por qué cambiar?
2. **Noviembre - diciembre:** lo que va a cambiar
3. **Enero:** lo que no cambiará
4. **Febrero:** los programas satélites y cómo será el proceso

## ¿Por qué cambiar?

### 1. Desventajas de la V1

Las primeras desventajas son principalmente visibles para los desarrolladores:

La razón principal que motiva esta migración: Dificultades para mantener Duniter V1 (corregir errores y realizar mejoras).

#### Problemas de seguridad:
- Duniter puede fácilmente ser bloqueado (ataque por saturación de la red).
- Todas las cuentas de miembros son potencialmente forjadores, sin embargo, algunas son fácilmente pirateables, lo que permitiría que un atacante comprometa potencialmente toda la blockchain.
- Obligación para los forjadores de realizar resincronizaciones manualmente para que siga funcionando.
- Riesgo de fork si los forjadores no actualizan Duniter rápidamente en caso de una corrección de errores.

[Haga clic aquí para saber: Qué es un fork?](https://journalducoin.com/lexique/fork/)  

Todos los usuarios ya han notado otras desventajas:

- **Lentitud en la ejecución.**
  - Tiempo largo para mostrar las operaciones.
  - Entre 5 y 10 minutos para que se procese una transacción, 30 minutos para que sea validada.
  
- **Problemas de sincronización de las piscinas:** Visiones diferentes de un nodo a otro, ya que ningún nodo tiene una visión global de las piscinas.
  
- Transacciones o certificaciones que no se validan.
  
- Certificaciones que desaparecen.
  
- Dificultad para que entren nuevos junistas (problemas de sincronización entre certificadores para tener 5 certificadores disponibles al mismo tiempo).

### 2. Ventajas de la v2

- Mantenimiento y actualizaciones más fáciles para los desarrolladores.
  
- Actualización automática sin fork para los forjadores (la posibilidad de rechazar una actualización seguirá existiendo, pero será voluntaria y no por olvido).
  
- **Un bloque cada 6 segundos**
  - Transacción validada en 30 segundos.
  - Tiempo de respuesta considerablemente mejorado.
  
- Sincronización de nodos más rápida y fiable.
  
- Certificaciones validadas inmediatamente (sin piscinas o pools).
  
- Entrada más fácil de nuevos miembros (ya no se necesita sincronización entre los 5 primeros certificadores).

### 3. Evolución del software, no de la moneda

El software evoluciona constantemente. A día de hoy, estamos en la versión 1.8.7 de Duniter y 1.7.13 de Cesium. Estas evoluciones se han realizado de manera que sean compatibles con las versiones anteriores y siempre han transcurrido sin problemas.

La versión 2 sigue siendo software libre; es una nueva blockchain que comenzará con todos los datos de la blockchain Ğ1 V1 en el momento del cambio (cuentas, transacciones, certificaciones...). Encontrarán de nuevo todas sus Ğ1, sus certificaciones y transacciones.

En cuanto a los datos de Cesium+ (perfiles, mensajería, notificaciones...), el trabajo está en proceso, ¡y tal vez no estarán desde el inicio!

Una nueva blockchain implica nuevos softwares (Cesium 2, Gecko, Tikka, G1nkgo 2, ...).

No será posible intercambiar entre dos blockchains diferentes; por lo tanto, después de la fecha de cambio, se
