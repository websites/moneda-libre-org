---
title: La moneda libre y las monedas locales
description: Diferencia entre la moneda libre y las monedas locales
---
## La moneda local complemetaria no es alternativa

No se crea de otra forma que de la moneda deuda: en muchos casos hay que pagar un euro para para obtener una unidad monetaria de ella e inversamente. La moneda local suele tener un valor equivalente al valor de la moneda fiat del país. Si el euro se desmorona o sufre una inflación, éstas también.

La moneda local no es una garantía de la localización de la economía, de la misma manera que el Euro (moneda local de la Zona Euro) no impide de ninguna forma la compra de productos procedentes de América o Asia.

Es muy facíl usar la moneda local en algunos supermercados de barrio para comprar plátanos o chocolate procedente de la otra punta del mundo. Estos supermercados de barrio usan la moneda "local" para atraer clientes y transforman al final gran parte de esta moneda en Euro. \
Al final la moneda local resulta ser unos bonos de compra o pagarés para una agrupación de tiendas o un colectivo.

Si de verdad quieres desarrollar la economía local, es más sensato mirar la procedencia de los productos que compras. Es mejor comprar manzanas en euros al productor de tu barrio, que plátanos de Centro América en moneda local en el supermercado del barrio (aunque sean ecológicos). 

## La moneda libre no es necesariamente local

Además, una moneda libre no es necesariamente local. Por ejemplo, la Ğ1, gracias a su infraestructura informática, es potencialmente mundial; de hecho ya funciona en más de 20 países. El único freno a esta expansión es la Red de Confianza (cuyas reglas pueden evolucionar).

## Una moneda libre puede ser local

Sin embargo, es posible crear una moneda local complementaria basándose en la moneda libre en vez del Euro. Se puede crear una moneda valorada con la Ğ1 o cualquier otra moneda libre (cuando haya más). \
Tal moneda sólo sería local por el nombre, porque se podría convertir fácilmente en moneda internacional.

También se puede concebir una moneda local independiente basada en la <lexique>Teoría Relativa de la Moneda</lexique>. Esa moneda sería realmente local, porque no sería convertible y sólo de uso local.