---
title: Comprender
description: Las explicaciones teóricas
plan: ui/main-flatplan
---
## La TRM

La moneda libre es la aplicación de la <lexique title="trm">Teoría Relativa de la Moneda</lexique> escrita en 2010 por Stéphane Laborde.

### 4 libertades

Esta teoría plantea 4 axiomas que son las 4 libertades económicas:

1. La libertad de elegir el propio sistema monetario
2. La libertad de utilizar los recursos
3. La libertad de estimar y producir cualquier valor económico
4. La libertad de intercambiar, contabilizar y mostrar tus precios "en la moneda"

La libertad se entiende, por supuesto, en el sentido de no dañar a uno mismo ni a los demás. Si hay daño, hay privación de libertad.

### Renovación de las generaciones

![Les humains du passé n'existent plus, les humains du futur n'existe pas encore](https://www.creationmonetaire.info/wp-content/uploads/2013/05/TRM35_6.png "Espace-Temps humain (espace de vie(t) des âges de 0 à ev en vert)")
Los humanos del pasado (en rojo) ya no existen, los humanos del futuro (en amarillo) todavía no existen. Los vivos (en verde) que llegan a final de su vida (ev≈80 años aprox.), han podido intercambiar con los humanos del pasado, pero nunca intercambiarán con los humanos del futuro. \
\
Los humanos al principio de su vida nunca han intercambiado con los humanos del pasado, pero intercambiarán algún día con los humanos del futuro.

Sólo los humanos vivos (en verde) pueden hacer intercambios entre ellos, no hay razones para que sus intercambios influyan en las futuras generaciones, tampoco para que sean impactados por los intercambios de las generaciones anteriores. 

### Igualdad espacio-temporal

Para respetar estas 4 libertades, es necesario que cada humano cree la misma parte de moneda (la misma porción).

Cada generación crea la moneda que usa sin que esta moneda tenga un impacto en las generaciones futuras.

### La fórmula

DU=c*(M/N)\
La parte creada por cada ser humano (DU) es una proción, un coeficiente (c) de la media de la masa monetaria por miembro (M/N).

El coeficiente **c** tiene que estar cercano al 10% anual para no privilegiar a los más jóvenes, ni tampoco a los más mayores.

### Convergencia de los saldos

Como cada creador crea la misma cantidad de moneda, sus cuentas se acercan **de forma relativa**. Tal y como una diferencia de edad se hace menos visible a medida que vamos envejeciendo.

![](/uploads/convergence-des-soldes.png)

## Más informaciones

#### La TRM.

Para saber más, puedes leer la Theoría Relativa de la Moneda. Este libro escrito por Stéphane Laborde está disponible on-line (fr/en) <http://trm.creationmonetaire.info/>

#### La TRM en detalle (resumen)

Emmanuel Bultot, Doctor en matemáticas, ha publicado ["la TRM en detalle"](https://foro.moneda-libre.org/t/teoria-relativa-de-la-moneda-trm-en-detalle/95), una  adaptación de la Theoría Relativa de la Moneda desde un nuevo punto de vista, muy bien hecha y fuertemente recomendada y traducida al castellano.

#### La TRM explicada para niños

David Chazaviel, ingeniero informático, ha realizado [«la TRM explicada para niños](http://cuckooland.free.fr/LaTrmPourLesEnfants.html), una explicación de la TRM interactiva más fácil de entender.

#### En Vídeo
Un vídeo muy explicativo de la TRM en 1h 15m. [Ver con subtítulos](https://foro.moneda-libre.org/t/subtitulado-la-teoria-relativa-de-la-moneda-por-stephan-laborde-2014/1935)


## La Ğ1

La Ğ1 (la "Juna") es la primera MONEDA LIBRE de la historia de la humanidad. \
\
De acuerdo a la Teoría Relativa de la Moneda (TRM), la juna se co-crea sin deuda, y a partes iguales, entre todos los seres humanos de todas las generaciones presentes y por venir, bajo forma de una "cesta" de monedas, una cantidad de Ğ1 (una cantidad de "junas"), llamada <lexique title="DU">DIVIDENDO UNIVERSAL</lexique> (DU).

¡Incluso niños y niñas, plenamente como seres humanos también participan en la creación monetaria creando su parte de moneda todos los días!

### El DUğ1

Para la moneda libre Ğ1 el DU se calcula cada semestre (182,625 días), la masa monetaria crece al 4,88% por semestre y se reparte cada día.

Cada día, cada co-creador crea una pequeña porción de Dividendo Universal.

Como la masa monetaria era cero al comienzo, el primer DU se fijó arbitrariamente en 10 Ğ1 al día y desde entonces ha ido augmentando.

### La fórmula adaptada

La fórmula de cálculo del  DUğ1 es: **DU<sub>t+1</sub> = DU<sub>t</sub> + (c<sup>2</sup> × (M/N)<sub>t</sub> / 182,625)**\
Este cálculo se hace cada 6 meses (182,625 días) y toma en cuenta la evolución del número de miembros durante la fase de instalación de la moneda.

### La blockchain

Se eligió a la tecnología blockchain por su facilidad de instalación, su seguridad y su descentralización.\
Al revés de lo que suele pensarse, no es la blockchain la que consume energía sino la prueba de trabajo.\
En la Ğ1 el algoritmo ha sido creado para [consumir muy poca energía](https://duniter.fr/faq/duniter/duniter-est-il-energivore/), un ordenador del tamaño de un paquete de cigarillos es suficiente para calcular los bloques, muy lejos de las enormes granjas que minan bitcoins.

### La red de confianza (o de reconocidos)

Al crearse la moneda en las cuentas de los usuarios miembros, hay que asegurarse de que cada uno tiene una sola cuenta única creadora de moneda.

Para respetar la descentralización, la privacidad y no dar el poder a ningún organismo, son los miembros mismos quienes identifican y reconocen a los otros seres humanos dentro de la red. 