---
title: La moneda libre y el medio ambiente
description: La Ğ1 soporta muy bien el decrecimiento y apenas gasta energía.
---
## El crecimiento

La moneda de los banqueros se crea mediante los créditos concedidos a los particulares y a los Estados. Los créditos se dan según la capacidad de reembolso, es decir la aptitud para obtener beneficios. Porque además, hay que pagar intereses. 
Esta moneda de los banqueros requiere un crecimiento perpétuo para seguir existiendo. Sin crecimiento ya no hay créditos, y sin crédito la moneda desaparece. 

La moneda libre se crea continuamente mientras haya seres humanos vivos. No necesita crecimiento, ni una carrera por los beneficios para seguir existiendo. 
De esta forma puede ser la moneda perfecta para acompañar un decrecimiento hacia un mundo más sostenible.


## El consumo energético

La Ğ1 no usa billetes infalsificables costosos en recursos: Prefiere usar la blockchain.\
El algoritmo de la Ğ1 fue cuidadosamente estudiado para consumir muy poca energía.\
<https://duniter.fr/faq/duniter/duniter-est-il-energivore/>\
La siguiente versión será todavía más económica en energía.