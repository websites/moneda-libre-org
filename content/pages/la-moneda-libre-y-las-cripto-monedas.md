---
title: La moneda libre y las criptomonedas
description: La Ğ1 es una criptomoneda, ¡pero no como las demás!
---
## Criptomonedas

Las criptomonedas son las monedas basadas en la tecnología de blockchain descentralizada.\
La mayoría de las criptomonedas conocidas están concebidas para ser especulativas: su creación o adquisición es a menudo reservada a las personas que tienen capacidades técnicas y financieras.

Lo que diferencia a la Ğ1 es su forma de creación. La Ğ1 se crea a partes iguales entre todos los miembros de la comunidad.

## Económica y ecológica

Imprimir billetes o acuñar monedas es una forma compleja y costosa de gestionar la moneda. En el momento actual, no se sabe hacerlo de una forma que sea lo suficientemente estable, segura y resiliente. El control de emisión de moneda en un soporte físico siempre está centralizado, y entonces puede ser corrompido o falsificado. 

Por eso la Ğ1 es una criptomoneda, es decir una moneda digital descentralizada y segura mediante cifrado, gracias a la tecnología blockchain.\
Además, Duniter (<i>de DU-unitario</i>) el software que genera el DU, que gestiona la moneda y la red de confianza, es un software bajo licencia libre: su código está abierto, ejecutable y verificable por cualquiera.