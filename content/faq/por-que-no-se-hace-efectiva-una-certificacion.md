---
title: ¿Por qué no se hace efectiva una certificación?
description: Ver las reglas de la lidencia
---
## Pausa de 5 días

Hay una congelación o pausa de 5 días entre dos certificaciones. Esta pausa o congelación comienza a contar en el momento de *hacerse efectiva una certificación*, es decir, en el momento que la certificación ya no queda en "espera de tratamiento". Este momento suele ser diferente de la fecha en que la certificación se ha emitido.

Puedes ver la fecha en las que tus certificaciones se hacen efectivas mirando las notificaciones en Cesium. Si hubieran certificaciones en cola se descongelarán después de 5 días. Si hay varias por entrar, es imposible determinar cuál será la siguiente que pasará, no hay orden de preferencia.

Es a partir de la fecha de emisión (no de hacerse efectiva), que se cuenta la caducidad de una certificación (2 años o 730 días y 12 horas). Por eso, se recomienda no poner en cola demasiadas certificaciones, ya que no serán tomadas en cuenta inmediatamente y seguramente esa cola no ayudará a posibles candidatos que necesiten tu certificación en algún momento específico (cuando otros 4 miembros estén libres).


## Un nuevo candidato debe tener 5 certificaciones

Y estas 5 certificaciones deben **estar disponibles (no congeladas) al mismo tiempo**. Si una de las 4 otras no está disponible, la candidatura no pasará.

Estas 5 certificaciones deben cumplir la <lexique>regla de la distancia</lexique> para que la candidatura se convierta en miembro.

Hay que esperar entonces que todas las certificaciones necesarias estén disponibles, o encontrar a otros certificadores.

## Un candidato debe publicar su petición de membresía desde Cesium

Se puede certificar una cuenta que solo ha publicado su identidad/usuario, pero sin haber publicado la petición de membresía.

Es decir, que solo ha rellenado el campo de *pseudónimo* en Cesium, un identificador único en Duniter que expira a los dos meses si no se convierte en miembro.

Normalmente la petición de transformación a cuenta miembro en Cesium, hace la publicación de identidad + petición de membresía. De igual forma si se crea una *cuenta miembro* directamente en Cesium hace las dos cosas.


## Los nodos pueden estar desincronizados. 

Si ves que la petición de membresía no se ha realizado, o si no véis las otras certificaciones de los miembros, quizás estés conectado a un nodo desincronizado o las acciones de los miembros han sido realizadas en algún nodo desincronizado.

**Antes** de todas las acciones con Cesium, es importante verificar que los nodos Duniter y Cesium+, a los que estás conectado están bien sincronizados. Puedes utilizar este [enlace](https://ginspecte.mithril.re/) para saber qué nodos funcionan bien.





