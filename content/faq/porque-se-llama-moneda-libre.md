---
title: " ¿Por qué se llama moneda libre?"
description: La moneda que se hace libre?
---
En la <lexique title="trm">Teoría Relativa de la Moneda</lexique> (TRM), Stéphane Laborde define la moneda libre desde el punto de vista que respeta las siguientes cuatro libertades económicas: 

1. La libertad de elección de su sistema monetario
2. La libertad de usar los recursos
3. La libertad de estimación y de producción de cualquier valor económico
4. La libertad de intercambiar, contabilizar y mostrar sus precios "dentro de la moneda"

**La libertad se entiende siempre por:**

* no dañar a los demás frente a uno mismo 
* no hacerse daño a uno mismo frente a los demás.

**En un sistema monetario libre:**

* la simetría espacial permite a un individuo evitar dañar a sus contemporáneos, 
* la simetría temporal permite que una generación evite dañar a las siguientes.

Las cuatro libertades fundamentales de la moneda libre están directamente inspiradas en las del software libre, tal como las define Richard Stallman. 
[](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Galuel_RMS_-_free_as_free_speech%2C_not_as_free_beer.png/800px-Galuel_RMS_-_free_as_free_speech%2C_not_as_free_beer.png  Stephane Laborde en compañía de Richard Stallman, creador del movimiento de Software Libre  para ilustrar la cita: \"Libre como en la libertad de expresión, no como cerveza gratis\"")
(fuente: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Galuel_RMS_-_free_as_free_speech,_not_as_free_beer.png?uselang=fr), licencia Creative Commons-by-sa)