---
title: ¿Cómo puedo encontrar a los usuarios?. 
description: Consultar los grupos locales o el mapa de miembros.
---
Existen varias soluciones:

## Solución n°1: contactar al grupo local

A menudo sucede que los miembros se organizan como "grupo local". 

Un "grupo local" es simplemente un grupo de seres humanos que eligen, por iniciativa propia, adquirir una o más herramientas para comunicarse entre sí y organizar reuniones.

El medio de comunicación elegido varía de un grupo a otro. Puede ser:

* un grupo de **discusión** por correo electrónico ("*mailing list*") 
* un grupo de *Facebook* un canal *Debate*, un *chat*, un *foro* o un *sitio web*
* O simplemente dentro del foro general de moneda libre, en su [categoría por región](https://foro.moneda-libre.org/) 

Utiliza el mapa de abajo para encontrar tu grupo más cercano y averigua qué canales usan para comunicarse:

<iframe width="100%" height="480px" frameborder="0" src="https://framacarte.org/fr/map/duniter-g1_8702?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=true&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=false&amp;searchControl=null&amp;tilelayersControl=null&amp;embedControl=null&amp;datalayersControl=null&amp;onLoadPanel=none&amp;captionBar=false&amp;datalayers=13342&amp;fullscreenControl=false#5/45.660/4.351" scrolling="yes" class="iframe-class"></iframe>

Ver este mapa de grupo en grande o Ver mapa de eventos y grupos locales a través de la información del foro.

[Ver este mapa de grupo en grande](https://framacarte.org/fr/map/groupes-locaux-et-rencontres-g1_8702)  
o [Ver mapa de eventos y grupos locales](https://carte.monnaie-libre.fr?members=false) a través de la información del foro.


### Solución n°2: contactar directamente a los miembros que vivan cerca 
La aplicación de Cesium tiene una opción que se llama directorio, que te permite ponerte en contacto directamente con los miembros que viven cerca tuyo. 

Para ello, cuando estés en Cesium, ve a la pestaña Directorio y luego a Mapa. 
Es mucho mas cómodo e intuitivo hacerlo desde el ordenador (que desde el móvil)

El mapa que reúne toda esta información es el de la página de inicio de este sitio. [Ver el mapa completo en pantalla completa](https://carte.monnaie-libre.fr/) 

También está el siguiente mapa: [world wotmap](https://worldwotmap.coinduf.eu/)

También puedes consultar el mapa online (simplemente ten en cuenta que no puedes iniciar sesión en tu cuenta ya que es una página con modo lectura): 
[Consulta el mapa de usuario de la demostración de Ğ1 en cesium](https://demo.cesium.app/#/app/wot/map?c=46.6042:3.6475:6). (este mapa actualmente puede tener errores).

Sólo aparecen en los mapas aquellos usuarios que opcionalmente en Cesium hayan marcado una área geográfica en su perfil, así que seguramente encuentres a mucha más gente en los grupos del foro, redes sociales o mensajería instantánea como Telegram, Matrix o XMPP. 