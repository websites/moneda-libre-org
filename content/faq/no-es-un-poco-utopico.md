---
title: " ¿No es un poco utópico? "
description: "Para nada: la Ğ1 está anclada en la realidad"
---
### La Ğ1 es un experimento concreto 

La G1 es un experimento que ya tiene unos años de existencia y de hecho ha impactado en la vida de cierto número de gente, a veces de manera negativa, pero principalmente de manera positiva. 

Algunos se implican demasiado rápido al principio y al final acaban dejándolo. 

Otros, aún pensando de partida que se trata de una estafa, por curiosidad experimentan con la moneda por sí mismos y se encuentran haciendo multitud de intercambios, encuentros y están encantados.  


### Lo mejor es darle tiempo para que crezca

Está claro que el proyecto G1 necesita tiempo para ganar adeptos, para ganar popularidad. 

De hecho, los desarrolladores de la G1 lo ven como algo a muy largo plazo (incluso de varias generaciones). 

A la espera de que los <lexique>junistas</lexique> cambien el mundo, los cambios se hacen uno a uno.
