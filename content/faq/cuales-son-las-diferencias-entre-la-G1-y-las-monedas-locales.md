---
title: "¿Cuáles son las diferencias entre la G1 y las monedas locales complementarias?."
description: Las monedas sociales son pagarés de Euros.
---

Cada unidad monetaria de una moneda local está asociada a un Euro, a veces respaldada en una cuenta bancaria, o simplemente como pagaré a devolver a la comunidad de intercambio local. Estas monedas son, en última instancia, solo euros disfrazados con un enfoque local que algunos ayuntamientos explotan. 

En algunos casos, para adquirir unidades de moneda local, normalmente es necesario comprarlas pagando moneda fiat, como en el caso de la moneda local de la famosa película ["El Milagro de Worgl"](https://tube.p2p.legal/w/beo4R51Dudsjr15YAAxFE8)

Para adquirir Ğ1 tienes que vender productos y/o servicios, pero todos pueden convertirse en co-creadores de moneda. 

Mientras que en España 1 moneda local = 1 €, la equivalencia Ğ1/Euro no está definida en ningún lugar.

En Francia las monedas locales(MLC) están autorizadas por [la ley del 31 de julio de 2014](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000029313554), están respaldadas por el Euro. 

Aunque suele ser una práctica poco necesitada, cuando los usuarios intercambian euros por Ğ1 o viceversa, aplican libremente el tipo de cambio que desean en el momento del intercambio, o suelen utilizar un banco de alimentos ecológicos para *descapitalizar* esos euros y convertirlos en junas.

Como anécdota, los ayuntamientos suelen gastar mucho más euros promocionando sus monedas locales (a través de salarios a funcionarios, congresos, publicidad, apps personalizadas) que la cantidad de moneda local intercambiada en ese municipio.

Las monedas locales *asamblearias* suelen centralizar todas las decisiones de la creación monetaria local (deuda máxima, saldo máximo, baneo de cuentas, etc.) en aquellos que controlen la asamblea con el paso del tiempo.

En España después del 15-M habían más de 300 monedas locales creadas y ahora mismo quedan alrededor de una decena poco activas.

También existen las monedas locales basadas en el tiempo (ej. horas), que suelen decidir una equivalencia para todo el mundo de x € = 1 hora de trabajo para poder vender productos y no sólo servicios. Suelen tener conflictos para ponerse de acuerdo en el valor de las horas de trabajo entre diferentes usuarios(profesionales y no profesionales, etc) ya que no todo el mundo suele pensar que la hora de trabajo tiene el mismo valor independientemente de cómo o quién.