---
title: ¿Por qué hay que utilizar un blockchain en la Ğ1?.
description: "¡Porque posibilita la autonomía y eficiencia energética!"
---
La impresión de billetes, o la acuñación de monedas es una forma compleja y costosa de manejar el dinero. En la actualidad, no sabemos cómo hacer esto de una manera que sea lo suficientemente confiable, segura y resistente. El control de la emisión de moneda en un medio físico siempre está centralizado y, por lo tanto, puede corromperse.

De ahí que la Ğ1 sea una criptomoneda, es decir, una moneda digital descentralizada y criptográficamente segura, gracias a la tecnología <lexique>blockchain</lexique>.
Además, Duniter (DU-uniter), el software que genera los DU, que gestiona la moneda y la web de confianza, es un software libre: su código es abierto, accesible y verificable por todos. 

Y además, el algoritmo de Duniter [consume muy poca energía](faq/es-la-g1-energivora)