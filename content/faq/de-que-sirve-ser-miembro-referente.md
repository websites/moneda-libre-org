---
title: ¿De qué sirve ser miembro referente?. 
description: Los referentes refuerzan la red de confianza
---
## Sirve de poco
Hay que tener en cuenta que los cocreadores <lexique>referentes</lexique> no tienen ningún derecho adicional, ni ningun privilegio, ni necesariamente son referentes en cuanto al conocimiento de la moneda libre. Se supone que son más activos en la red de confianza, ya que han recibido y emitido suficientes certificaciones.

Es un término técnico usado por la blockchain para poder saber qué miembros están mínimamente activos en la red de confianza, y evitar que zonas con muchos miembros pero poco activos puedan bloquear el crecimiento en zonas que comienzan.

## Sobre la regla de la distancia. 
Para medir <lexique>regla de la distancia</lexique>, el software Duniter lo hace solo a partir de las personas que hayan emitido y recibido un cierto número de certificaciones, en el Léxico podrás aprender más sobre las certificaciones necesarias para ser miembro <lexique>referente</lexique>.