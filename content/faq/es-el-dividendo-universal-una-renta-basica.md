---
title: ¿Es el Dividendo Universal una Renta Básica Universal (RBU)?.
description:  ¡Renta básica, pero no de subsistencia!
---

# Renta vital o renta universal

En una moneda libre, el <lexique>Dividendo Universal</lexique> (DU) es el único medio por el cual se pueden crear unidades monetarias. 

En este sentido, podemos decir que es una renta vital, ya que es el cimiento (la “base”) de la creación monetaria. 

El DU es creado por todos (por “la base”) de manera individual y con la única condición de estar vivos, no es creado solo por quienes están en la cima de las pirámides monetarias. Para la moneda libre Ğ1, todos los miembros de la red de confianza crean el DU todos los días. En este sentido, podemos por tanto decir que se trata de un “ingreso universal” (al menos para los miembros de la red de confianza).

# ¡Pero no de subsistencia!

Por otro lado, si es posible o no vivir con el DU no está definido en el código monetario de una moneda libre.  

La cantidad de DUs dependería de cada persona, ya que no todos tienen las mismas necesidades. 

Hasta donde sabemos, nadie ha logrado subsistir solo con su DU. 

Quizás en unos años, cuando la moneda libre Ğ1 tenga muchos más miembros que produzcan bienes y servicios, y la competencia entre productores haya bajado los precios de mercado, será posible vivir solo de DUs. Actualmente, no estamos allí.

[rdb]: https://fr.wikipedia.org/wiki/Revenu_de_base#:~:text=Le%20revenu%20de%20base%2C%20encore,obligation%20ou%20absence%20de%20travail.
