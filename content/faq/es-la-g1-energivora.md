---
title: ¿es la G1 "energívora"?
description: "La moneda G1 es eficiente, energéticamente hablandos"
---
Los servidores que gestionan la moneda corren perfectamente en ordenadores tan modestos como el RaspberryPi, que es un nanoordenador que consume (solo) 5W.

Esto hace que la Ğ1 sea una moneda low-tech (baja tecnología), muy eficiente energéticamente, con una huella energética muy baja (mucho más baja que la del €, e infinitamente más baja que la de Bitcoin donde gigantescas granjas de computo compiten entre sí para la creación monetaria). 

Mas información en la página de Duniter [es energívora?](https://duniter.fr/faq/duniter/duniter-est-il-energivore/) (en francés de momento).