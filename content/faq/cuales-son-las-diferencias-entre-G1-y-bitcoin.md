---
title: "¿Cuáles son las diferencias entre la G1 y el Bitcoin?."
description: La creación del  Bitcoin se basa en potencia de procesamiento (de cálculo). La G1 no es especulativa.
---
## La creación monetaria

Los bitcoins se crean recompensando a los dueños de los ordenadores que hacen funcionar un nodo de la blockchain. 
La creación monetaria se basa en una potencia de procesamiento, de allí su fuerte consumo energético. 

Con el tiempo se crean cada vez menos bitcoins. Hacia el año 2140 ya no habrá creación de bitcoins, habrá que utilizar los que ya estén creados. Además, los primeros "mineros" han podido crear bitcoins mucho más facilmente que los "mineros" de hoy, entonces no hay igualdad de creación monetaria en el tiempo. 

Al revés que la moneda libre Ğ1 que se crea a partes iguales entre todos los co-creadores (que hagan funcionar la blockchain o no). El humano está en el corazón de la creación monetaria. 
Con el tiempo se crean cada vez más Ğ1 (proporcionalmente a la masa monetaria). 

Donde el bitcoin favorece a los que están capacitados para hacer funcionar un <lexique>nodo</lexique> (técnica y financieramente), la Ğ1 no favorece a nadie. 

## La Ğ1 no es un activo especulativo

La escasez programada del bitcoin hace que sea un candidato a la especulación. Cuánto más raro más caro (en relación con las otras monedas de media). 

Por el contrario, la moneda libre, al ser cada vez más abundante en unidades, no puede ser realmente una reserva de valor a largo plazo.

Además, como el metro, el gramo o el segundo, la moneda libre es un verdadero instrumento de medida de valores económicos fiable en el tiempo, usando el <lexique>DU</lexique> como unidad comparativa. 

## Tiene la Ğ1 puntos en común con el Bitcoin? 

Como el bitcoin, la Ğ1 usa una blockchain.
Pero se ha mejorado el algoritmo para ser un [bajo consumidor de energía](/faq/es-la-g1-energivora)

Pero **la moneda libre no es sólo la Ğ1**.
El concepto de moneda libre es independiente de la tecnología usada. 