---
title: Se puede certificar con los documentos de identidad?.
description: No es útil
---

El carnet de identidad - emitido por el Estado - se puede usar para aumentar la confianza de que una persona no está suplantando a otra, sin embargo:
* exigirlo sería negar el derecho al anonimato o al seudónimo;
* el Estado se niega a dar papeles a ciertas personas, que por lo tanto serían injustamente excluidas del Ğ1;
* confiar demasiado en estos documentos les daría un enorme poder sobre el Ğ1;
* la moneda se volvería aún más vulnerable a las inestabilidades estatales;
* francamente, ¿le gustaría reemplazar un sistema social con una burocracia antidemocrática y anti-dinero libre?

La certificación puede dar una apariencia “sectaria” a la red de confianza. Sin embargo, ¿qué es lo que está haciendo a parte que encuestar y controlar a la población mediante el uso de la fuerza, principalmente para recaudar impuestos e imponer sus políticas?. Así que, al certificar a un familiar, simplemente decimos “esta persona existe y ha entendido y aceptado la licencia”. 
¡Estamos lejos de la burocracia estatal! 

Que si lo pensamos un poco, es el mismo razonamiento que se aplica a cualquier proveedor de identidad centralizado, como GAFAM (Google, Amazon, Facebook, Apple, Microsoft).