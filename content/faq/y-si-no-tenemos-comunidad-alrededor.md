---
title: ¿Y si no tenemos comunidad alrededor?
description: "Sé tu el primero."
---
## Creemos una comunidad. 
La moneda es antetodo un medio de intercambio. 
Así que estando solo esta moneda no tendrá mucho sentido, es decir, no servirá para mucho.
Hay que formar grupos de personas para poder hacer intercambios.

## Ampliemos nuestros conocimientos.
Sin salir de casa hoy en día se puede participar en videoconferencias. Por ejemplo, recomendamos [esta instancia libre de Jitsi](https://meet.guifi.net/) que no necesita instalar ningún software y funciona directamente en el navegador.
Todo ello para aprender más sobre la moneda, además de conocer a otros junistas. 

No te sientas tentado a pedir certificaciones mediante videoconferencia, sólo por haberle visto la cara a alguien. Aprovéchalas para aprender más, motivarte y acudir o generar tus propios encuentros físicos de moneda libre. Esto facilitará su certificación en un futuro coincidiendo en algún encuentro por ejemplo, sobre todo si previamente ya os conocéis y  habéis mantenido conversaciones escritas o de voz y tenéis conocidos en común. 

## Atraigamos al mundo. 
En la plataforma [gchange](https://www.gchange.fr/) y [girala.net](https://www.girala.net/) se puede publicitar bienes o servicios, para poder empezar a hacer intercambios. 
Si además se da el caso de dispones de espacio en su vivienda, se puede proponer alojamiento en [airbnjune](https://airbnjune.org/). Éste es una buena manera de tener a junistas en tu casa

## Hablemos en nuestro entorno
Puedes hablar con tus amigos, con las asociaciones de tu entorno. Si no conoces la moneda libre, lo mejor es aprender con las personas que la utilizan.
Tambien puedes hablarlo con tus amigos, con las asociaciones o centros sociales de tu entorno. O con los grupos de monedas locales que hay o habían en tu zona, donde encontrarás gente que ya conoce los problemas actuales del sistema económico.

Puedes proponer reuniones cerca de ti, estas reuniones deben indicarse en [el foro](https://foro.moneda-libre.org/).
Comienza con reuniones en lugares públicos: bar, parque u otros. No siempre tendrás las respuestas a todas tus preguntas, pero os conoceréis (esto será útil para futuras certificaciones). 

Debatid sobre los temas de la moneda libre y mencionad vídeos y páginas que pueden proporcionar respuestas a vuestras preguntas. 

Con este pequeño grupo, puedes compartir el coche para ir a las reuniones o mercados un poco más alejados. Y finalmente, conocerás a las personas con las que hablaste por videoconferencia.

## Como no perderse en un encuentro
En el foro hay un tutorial corto hecho por un bretón ["Como no perderse una reunión"](https://forum.monnaie-libre.fr/t/comment-ne-pas-rater-une-rencontre/7408), que te podría servir y lo podrias adaptar a tu región. 
Ten en cuenta también los mensajes anclados en la parte superior que contienen enlaces o listas de correos donde suscribirse

## Va a ser un proceso largo
No te desanimes si en tu primera reunión sois pocos. Piensa que te permitirá saber qué pasa en tu comunidad. Pasará tiempo hasta que seáis diez personas. ¡Seréis pioneros, ánimo!

*O también puedes esperar tranquilamente a que los demás hagan todo este trabajo de creación de comunidad por ti... y será aún más largo.* 