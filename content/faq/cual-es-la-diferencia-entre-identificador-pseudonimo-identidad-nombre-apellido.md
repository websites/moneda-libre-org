---
title: ¿Cuál es la diferencia entre identificador, pseudónimo, identidad, nombre apellido?. 
description: Depende para que lo uses
---
## Identificación secreta

Esto es lo necesitas para iniciar una sesión con su cuenta, que no debes decir a nadie. Al igual que tu frase de contraseña. 
**Nadie más que tú debería conocerlos.**

<alert type="danger" icon="skull-crossbones">Si tu lo cambias (este nombre de usuario o contraseña) cambias de cuenta..</alert> 


## Pseudónimo (id-duniter)

El campo "seudónimo" en Cesium está mal llamado como tal, de hecho es el identificador de Duniter. Este identificador es único. Una vez validado, **nunca se puede cambiar.** 

El permite que tus socios te encuentren cuando te busquen en el directorio. 
*Es una especie de identificador público*.

**Solo las cuentas de co-creador tienen un id-duniter validado.**

<alert type="warning">Incluso si la identificación sugerida es FirstNameName, mejor si eliges un seudónimo, porque esta información es pública y para proteger tu privacidad es aconsejable permanecer discreto en Internet.</alert>


## Identidad (Id-duniter + fecha)

Cuando solicitas la transformación de tu cuenta en una cuenta de co-creador (cuenta de miembro), **o** cuando solicitas la "creación de una cuenta de miembro", estás haciendo dos cosas: (1) publicas una identidad **y** (2) solicitas una membresía. 
Esta identidad será validada cuando hayas obtenido las 5 certificaciones, respetando las reglas de la <lexique nom=TdC>red de confianza</lexique>.  
Si una identidad no se valida en 2 meses (60 días y 21 horas), desaparece de la cola temporal del nodo (conocida como *pool*). Entonces tienes que empezar el proceso de nuevo.


### Doble identidad

Siempre que no se valide una identidad, es posible publicar otra identidad en la misma cuenta (conectándose a un nodo cuyo grupo no esté completamente sincronizado). 
Esto significa que una cuenta puede tener temporalmente una doble identidad (no validada). 
Tan pronto como se valida una de las identidades, la otra desaparece. 

Ten cuidado a la hora de guardar el [fichero de revocación] correcto, de hecho es aconsejable volver a descargarlo para mayor seguridad.


## Apellido, nombre (puedes poner un alias)

El campo "apellido y nombre" forma parte de los datos de cesium+ como todos los datos de perfil, que también son públicos.
Es por eso que no se recomienda poner tu verdadera identidad allí. 
Estos datos se pueden modificar facilmente editando tu perfil. 

<alert type="warning">Al tratarse de campos editables, sin ningún control, es fácil pretender ser un perfil que no es (como ocurre en las redes sociales). Así que es aconsejable **verificar siempre la llave pública** que es única.</alert>
