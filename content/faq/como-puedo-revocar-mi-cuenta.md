---
title: ¿Cómo puedo revocar mi cuenta?. 
description: Esto es únicamente válido para las cuentas cocreadoras de la moneda.
---

## Únicamente para las cuentas cocreadoras de moneda
Revocar una cuenta conlleva que ya no podrá crear dinero ni emitir certificaciones, así que no tiene mucho sentido revocar una cuenta que no genera dinero. Únicamente que Cesium evitará la certificación en una cuenta con una solicitud de revocación pendiente.

## A través de las opciones en mi cuenta"
Si tienes acceso a tu cuenta a través de tus contraseñas no te hace falta el documento de revocación. Pero si quisieras revocarla (porque quizás alguien hackeó tus contraseñas débiles), ve a "Mi cuenta", pincha en "opciones", después en "cuenta y seguridad..." y finalmente en "Revocar esta cuenta de inmediato".  

Este procedimiento es útil cuando creas que tu nombre de usuario y contraseña puedan haber sido robados o pirateados, o crees que no son lo suficientemente seguros, cuando quieras cambiar tu "seudónimo", es decir, ID de Duniter.

## Por el enlace "Olvidé mi contraseña"
Cuando hayas perdido tus contraseñas, en la pantalla inicial, cierra la sesión y en la pantalla de inicio cuando quieres identificarte hay un enlace debajo de los botones que pone "¿olvidó su contraseña?". 

Pincha y podrás elegir entre «recuperar mi contraseña» (sólo si creaste en su momento un archivo de copia de seguridad de tus contraseñas) o «revocar mi cuenta de miembro» (desde un archivo de revocación).