---
title: ¿Cómo sé a qué precio vender?
description: Hay diferentes métodos a la hora de definir el precio de venta ....
---
## Hay varias opciones para ayudarte a decidir tu precio de venta:
 
1. **Por precio de mercado:** puedes buscar en ğchange **el precio medio** al que se vende el producto o servicio que ofreces.
2. **Por precio relativo:** puedes buscar en ğchange el precio de los productos o servicios que te gustaría obtener y establecer el precio de lo que estás vendiendo en relación con tu estimación del valor, en relación con el valor de lo que quieras obtener. 
   *Por ejemplo:* supongamos que quieres comprar cómics y vendes mermelada. Si encuentras que un cómic vende por 10 UD (en promedio) y estimas que tu tarro de mermelada vale medio cómic, entonces puedes intentar vender tu tarro de mermelada por 5 UD. Ahora eso sí, nadie te asegura que finalmente vendas tus mermeladas a este precio. Hay que tener en cuenta que **todo valor es relativo**: lo que es valioso para ti, puede ser menos valioso para una persona y más valioso para otra tercera. Probablemente estemos sujetos a ese apego, que nos hace tender a sobrestimar el valor de lo que tenemos.
3. **Por subasta:** si crees que tu producto/servicio es lo suficientemente demandado por la comunidad, siempre puedes organizar una subasta para obtener el máximo de Ğ1 en el intercambio. Esta opción te evita perder tiempo pensando en el precio de venta,  ya que simplemente dejas que “el mercado” decida qué precio está dispuesto a pagar por tu producto/servicio. Para simplificar las cosas, consulta la [subasta de Vickrey.](https://fr.wikipedia.org/wiki/Ench%C3%A8re_de_Vickrey)
4. **Por precio libre:** Basta con poner en tu anuncio "Precio Libre", para que sea el comprador quien determine el monto que desea pagar por tu producto y/o servicio. En general, resulta que los compradores son mucho más generosos y te ofrecen precios más altos de lo que hubieras imaginado pedir. 
*¡No es raro que te ofrezcan 3 veces más de lo que te hubieras atrevido a pedir!*