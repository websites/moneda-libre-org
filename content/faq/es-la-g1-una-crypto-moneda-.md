---
title: " ¿La G1 es una criptomoneda?."
description:  Si, pero no ....
---
Si quieres, se puede llamar "criptomoneda" a la moneda libre Ğ1, ya que al final la cadena de bloques se basa en tecnologías de criptografía. 

Pero eso sería olvidar que la criptografía no es prerrogativa ni de la cadena de bloques, ni de las monedas que la prensa generalista llama “criptomonedas”.

No hay que olvidar que la criptografía sirve también para asegurar cualquier transacción digital, ya sea en euros o en cualquier otra moneda. 

Pero una moneda libre podría administrarse igualmente con papel o incluso garbanzos. Desde luego sería menos práctico y probablemente requeriría - al final - de un organismo centralizador.