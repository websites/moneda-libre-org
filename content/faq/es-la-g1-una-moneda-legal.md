---
title: ¿La G1 es verdaderamente una moneda?
description: Sí, pero no de uso legal
---
Si se define una moneda como un facilitador de intercambio, una unidad de medida de valor, entonces **sí**.  

La ley no reconoce la Ğ1 como moneda, ni el Bitcoin ni las monedas locales. Sin embargo, la ley es normativa y no descriptiva, y no es suficiente ni relevante para describir. 

La moneda libre Ğ1 no se materializa en billetes. Esto no significa que no sea una moneda: la moneda no es necesariamente algo material, la mayoría de los euros son solo cifras en las cuentas. Además, los billetes no son dinero estrictamente hablando: son solo reconocimientos de deuda que emite el banco, ¡los euros "reales" son valores informáticos en los ordenadores de los bancos!