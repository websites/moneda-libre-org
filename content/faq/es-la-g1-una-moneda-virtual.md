---
title: "¿La G1 es una moneda virtual?."
description: "Virtual es únicamente lo que no existe?"
---
La moneda libre Ğ1 existe, así como las miles de transacciones realizadas por sus usuarios, desde su creación el 8 de marzo de 2017. 

El hecho de que las junas utilizadas estén almacenadas en ordenadores no las convierte en virtual. 

El 95% de los euros en circulación existen en formato digital. Si te cuesta ver esta cantidad, cuenta los euros que tienes en tu cartera y los comparas con el total de euros que posees: seguramente no es ni el 5% de todo lo que tienes, ¿verdad?. 

Eso sí,  en ningún caso todos los euros que aparecen en los saldos de tus cuentas corrientes y de ahorro tienen su contrapartida en forma de billetes o monedas, y menos aún en lingotes de oro. ¿Son por lo tanto “virtuales”?.

Hay que entender que el saldo de tu cuenta bancaria es la cantidad de dinero que el banco te debe. Esto no significa que posea esta moneda. Así que viéndolo más en detalle, ¡al final la que es moneda virtual es el euro!