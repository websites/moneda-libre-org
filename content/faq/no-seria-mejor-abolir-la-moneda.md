---
title: ¿No sería mejor abolir la moneda?. 
description: La moneda ha existido bastante antes que los bancos.
---
Eres libre de abolirla por ti mismo si lo deseas, pero probablemente habrá pocas personas dispuestas a seguirte.

## En una pequeña comunidad

Incluso cuando no usamos dinero, es raro que no contemos con él. 

Algunas comunidades presumen de vivir “sin dinero”, pero siempre hay un libro de cuentas en alguna parte, al menos en la cabeza de cada miembro de la comunidad. 

Este tipo de comunidades “sin dinero” existen en condiciones muy específicas: suelen ser muy pequeñas y aisladas del resto del mundo, geográficamente por ejemplo.

## Entre amigos

Imaginemos que intercambiaramos servicios entre “conocidos”, ¿de cuántos estaríamos hablando? ¿de 150 como mucho?.

Incluso entre vecinos, los intercambios pueden crecer. 

Igualmente, en los barrios o pueblos hacemos favores entre vecinos, y si das las gracias, probablemente escucharás un: *"no se merecen"*. 

Porque decir “gracias” es una forma de extinguir una deuda. Mientras que todos sabemos que muchas veces es: *"hoy por tí, mañana por mí"*

## La moneda: ¿fuente de todas las palabras?

*Idealmente, el dinero debería ser solo un medio para medir los intercambios, de forma totalmente neutral*.
*¡Y eso es lo que hace el moneda libre!*

Ciertamente, esta no es la realidad del momento, en cambio si le dices a un  <lexique>junista</lexique> que: *el dinero lo arruina todo*  probablemente te responda que: *depende del sistema monetario* 

o incluso que: *no es de extrañar dejar de lado la conciencia para aceptar el dinero, cuando se vive en un sistema de dinero-deuda donde la falta de dinero nos condiciona a todos*