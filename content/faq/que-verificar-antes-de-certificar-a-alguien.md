---
title: ¿Qué verificar antes de certificar a alguien?
description: La licencia ha de cumplirse
---

## Cumplimiento de la licencia.
Es importante que se respete [licencia de la G1](/faq/licencia-g1) para que la moneda siga siendo confiable. Porque es la confianza la que da "valor" a una moneda. Por lo tanto, debe asegurarse que la persona haya leído y entendido la licencia.

## Conocer a la persona.
Aquí depende de lo que cada uno entiende por "conocer" a una persona. Hay muchas pistas en la licencia. Se trata sobre todo de que podamos, en caso de duda, comprobar que la persona no tiene dos cuentas de cocreador.

## Asegúrate de que domina su cuenta.
Tienes que estar seguro de que la persona sabe cómo conectarse a su cuenta. La forma más fácil es enviarle algunos Ğ1 y pedirle que te los devuelva o que los transfiera a la cuenta de su elección.
Si la persona no puede hacerlo, no la certifiques, comprueba con ella en qué se está equivocando.

## El documento de revocación.
Actualmente, un error impide descargar el documento de revocación desde un teléfono.
Si la persona no sabe dónde está su documento de revocación, no es necesario certificarlo inmediatamente, sino ayudarlo a descargar y guardar este documento.

## Si la cuenta ya es miembro.
**Aun así tienes que hacer estas comprobaciones**, porque hay muchas personas que confían en otros sin comprobar las cosas por si mismas.
Nos podemos encontrar con que la persona no recuerde dónde guardó su documento de revocación; o que todavía tiene lagunas en la comprensión de la licencia.

## Calidad antes que cantidad.
Es mejor tener una pequeña red de alta confianza que una gran red de poca confianza.
No hay prisa por convertirse en co-creador, lo más importante es entender cómo funciona la moneda libre.
Si el usuario participa regularmente en las reuniones, se dará a conocer y obtendrá sus certificaciones sin dificultad.