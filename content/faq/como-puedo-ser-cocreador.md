---
title: ¿Cómo puedo ser cocreador?
description: "Procedimiento para cocrear moneda"
---
## Sin prisa

Para poder utilizar la moneda libre no es indispensable ser cocreador. 
Basta con abrir un monedero para comprar/vender en junas. 
Tan importante es circular la moneda, como crearla.

## Lo primero conocer gente

Para hacerse miembro crocreador, éste primero tiene que estar identificado - en calidad de ser humano (vivo) - por otros cocreadores que le conozcan. La mejor manera de darse a conocer es participar en todos los encuentros que pueda, con el fin de discutir y hacer intercambios. Desde el momento que los cocreadores le conozcan, entonces ya se podría pedir que le certifiquen.

## Comprometerse

El futuro cocreador ha debido dedicar tiempo a leer e interiorizar bien la [licencia de la G1](/faq/licencia-g1)
Debe comprometerse a respetar y hacer respetar esta licencia. 
Comprometerse, entre otras cosas, a no certificar ni re-certificar al que no respete esta licencia.

## Las cinco certificaciones

Para convertirse en miembro hace falta recibir (al menos) cinco certificaciones y respetar la <lexique>regla de la distancia</lexique>. 
Es conveniente tener cinco cocreadores dispuestos para la certificación (las cinco promesas), antes de empezar con todo el proceso. 
Para cumplir la regla de la distancia es preferible tener conocidos de comunidades distintas.

## Seguir haciendo encuentros

Convertirse en miembro no es un objetivo como tal, es un principio. Es importante seguir acudiendo a los encuentros para hacer circular la moneda, para encontrarse con nuevos miembros y para seguir recibiendo certificaciones. 