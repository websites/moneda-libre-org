---
title: "¿Es la G1 segura?"
description: Sí, gracias a la blockchain
---

La moneda libre Ğ1 se basa en la tecnología <lexique>blockchain</lexique>, o cadena de bloques en castellano. 

Es como un gran libro de contabilidad firmado con tecnologías de criptografía. 

**Una blockchain es infalsificable** ya que cualquier manipulación en alguno de los bloques hace que la frase de verifición del bloque siguiente no coincida y hay redundancia con muchos otros nodos duplicando y compartiendo la cadena original.

Los creadores del Ğ1 eligieron esta tecnología porque tiene muchas ventajas: 
* Consumo energético bajo. 
* Transacciones infalsificables.
* No hay una entidad central que controle la red o la moneda. 
* Siempre que el usuario elija frases de contraseña lo suficientemente largas y complicadas, su dinero no puede ser robado.