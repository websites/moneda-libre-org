---
title: ¿Por qué se crea la moneda todo el tiempo? 
description: Para una creación monetaria sostenible.
---

## Contra la desigualdad. 
Si decidimos irnos con la misma cantidad de dinero, con el tiempo unos captarán más dinero que otros. Y cuanto más dinero tengas al principio, más dinero podrás capturar. Con el tiempo las desigualdades se ampliarán, hasta que el sistema se bloquee porque unas pocas personas habrán capturado todo el dinero.

## Por el relevo generacional. 
Por el relevo generacional. El fenómeno se agrava con la renovación de generaciones. 
Aquellos cuyos padres ganaron mucho dinero, comienzan en la vida con una gran ventaja sobre aquellos cuyos padres no podían ganar tanto. 
La moneda de los bisabuelos afecta a sus bisnietos, a pesar de que nunca se han conocido.

## Para evitar sacudidas. 
Si decidiéramos crear una cantidad fija por persona, tendríamos que saber a qué edad crear este dinero. 
Es imposible determinar una edad, cada individuo se emancipa a una edad diferente. 
Si un individuo desaparece prematuramente, su parte del dinero sería demasiado grande en comparación con el uso que tuvo de él. La influencia de esta parte del dinero sería demasiado grande. 
La creación de dinero sería caótico.

## Creación continua
La creación continua hace posible nunca quedarse sin dinero, comenzar de nuevo en la creación y tener una evolución suave y predecible de la oferta monetaria.