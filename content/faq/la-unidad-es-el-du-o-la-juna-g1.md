---
title: ¿ La unidad es el DUğ1 o la Juna Ğ1 ?
description: "La Ğ1 para las cuentas, el DUğ1 para intercambios duraderos"
---
## El DUğ1 es la invariante temporal.

Cuando hablamos de igualdad espacio/tiempo en la moneda libre, contamos en DUğ1. Cada ser humano creará tantos DUğ1. 

El DUğ1 es una pequeña porción de la masa monetaria global, que se reevalúa cada seis meses para representar siempre el mismo porcentaje de la masa monetaria media global. Cualquiera que sea el monto del DUğ1, siempre representará un día de creación monetaria individual.

## La Ğ1 son cifras en una base de datos descentralizada.

Cada DUğ1 es una cantidad de Ğ1, no es una transacción en la cadena de bloques ya que no viene de nadie ni de nada, se crea automáticamente por parte de cada miembro cocreador en sus cuentas. Es un concepto innovador que no tienen la gran mayoría de criptomonedas que usan recompensas de minado (consumo de energía) o de acumulación (almacenamiento o *stake*) para crear la moneda de forma no equitativa.

## El DUğ1 como unidad de intercambio.

Al poner tus precios en DUğ1 ya no necesitas pensar en el aumento de la oferta monetaria, ya que con su revaluación semestral tus precios siguen el aumento de la masa monetaria "de forma natural". Esto es muy útil para servicios ofrecidos permanentemente en el tiempo (ej. un alojamiento o un servicio de traducción). Como símil, sería como poner tus precios en unidades del salario mínimo de tu país (ej. SMI en España) en lugar de unidades monetarias (euros), excepto que el salario mínimo no es realmente un invariable económico. 

El DUğ1 servirá a nuestros descendientes a comparar precios a lo largo del tiempo. Es difícil comparar los precios de hoy con los del pasado, la oferta monetaria era diferente, los salarios también, y el valor de la moneda también. Nos falta una unidad de valor común. 

*El DUğ1 es nuestra unidad de valor común a través del espacio y el tiempo.*

## Configurando Cesium. 

En los ajustes de Cesium, puedes elegir la visualización en DUğ1. Hazlo y verás que tus intercambios del pasado tienen cada vez menos valor, respecto al DUğ1 actualizado. De momento no está disponible una pantalla DUğ1 antigua vs DUğ1 actualizada (quizás algún día...).
 