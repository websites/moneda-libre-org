---
title: ¿Cómo la Ğ1 crea el vínculo social?
description: Tienes que conocer a los otros miembros para participar.
---
### Enlace social
Para garantizar a toda la comunidad que cada persona solo creará su parte de moneda una sola vez, se necesita un sistema de identificaciones. De ahi que se haya creado una red de confianza. Es un sistema de identificación descentralizado

Cada nuevo miembro debe de ser conocido y reconocido por cinco personas que ya son miembros, que son quienes garantizan la unicidad de la cuenta en la que se genera el DU diario del nuevo miembro. 
Este reconocimiento solo puede ser realizado entre seres humanos reales, y no por máquinas o una entidad central encargada de emitir documentos de identificación, así las personas se conocen y se encuentran, reforzando así el vínculo social y las oportunidades para realizar intercambios.