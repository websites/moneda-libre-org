---
title: ¿Los primeros miembros tienen algún privilegio respecto a los recién llegados?
description: Sí, pero no.
---

## Hay que empezar por vender

Si llegas a un mercado donde algunas personas tienen moneda pero tú no, lo primero que tendrás que hacer es vender antes de poder comprar. 

Esos usuarios que han estado produciendo Ğ1 durante algún tiempo, tienen la ventaja sobre ti de poder comprar antes de vender. **Es más, si ya han vendido muchos productos o servicios, se puede dar el caso de que puedan comprar incluso sin vender.**

Pero si lo hacen indefinidamente, el saldo de su cuenta lógicamente tenderá a 0. En realidad, esta manera de actuar pronto desaparece como práctica habitual; sobretodo si los precios representan una cantidad significativa respecto a los días de creación monetaria. 

Cogemos este caso, si un tarro de mermelada cuesta 6 DU, con los 30 que genera al mes, sale que un miembro de la web de confianza Ğ1 puede comprar, al mes, 5 mermeladas sin vender nada. 

¿Merece la pena?

## Puedes comprar a crédito

Además puede ocurrir que si en un momento dado te encuentras sin Ğ1 y quieres comprar algo en un <lexique title="gevento">ğmercado</lexique>, por ejemplo, que el vendedor acepte bajo acuerdo mútuo que le pagues más tarde.

*También puede pedir prestados Ğ1s de otros usuarios o de los responsables del mercado.*

### ¿Por qué no lo aceptaría?

Sabe de sobra que vas a producir Ğ1 a corto plazo y no hay razón para dudar de tu capacidad de pago. 

## Finalmente, todos crean el mismo recurso compartido

Al final todos crean la misma parte, es decir, con la convergencia de cuentas, todos los miembros habrán creado la misma porción de moneda (la misma cantidad de DUğ1), independientemente de su fecha de ingreso a la co-creación de moneda.

![](/uploads/convergence-des-soldes.png)