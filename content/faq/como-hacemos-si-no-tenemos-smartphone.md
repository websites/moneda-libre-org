---
title: ¿Cómo hacemos si no tenemos smartphone?
description: En casa y afuera, recibe y compra
---

## Si tenemos ordenador
Cuando estés en casa, administra tu cuenta desde el ordenador con la [aplicación Cesium](/ressources?filters=App).

Cuando no estés en casa y quieres pagar o cobrar por intercambios en un ğmercado, lo mejor es que imprimas tu código QR. 
Dentro de Cesium pincha en el código QR para ampliarlo, captura pantalla y lo imprimes. Es conveniente recortar la imagen antes de imprimir.
Si no tienes impresora, seguro que encuentras a alguien que lo puede hacer por ti y con varias copias que tengas es suficiente.

Dos opciones para hacer compras:

1. Muéstrale al vendedor que puede confiar en ti, dándole tu correo electrónico, tu móvil o que te identifique por Cesium. Y le mandas tu llave pública. 
   No se aconseja copiar a mano la llave publica porque hay demasiado riesgo de equivocarse. 
   **Ya le pagarás cuando llegues a casa.**

2. Imprime billetes.
   Esta opción consiste en crear monederos de papel que acreditas con DUğ1. https://foro.moneda-libre.org/t/billetes-para-encuentros-y-mercadillos/484 o https://forum.monnaie-libre.fr/t/le-g1billet-nantais/14977 
   Es un poco técnico, asi que igual mejor si te apoyas en alguien que sepa informática en tu localidad. 
   Cuando tengas los billetes en papel, se los das al vendedor del puesto para que pueda transferir a la cuenta que considere. 
   Ojo a no imprimir el mismo billete dos veces.

## Si no tenemos ordenador.
Para poder crear y gestionar cuentas Ğ1 necesitas un ordenador o móvil.
**Puedes acercarte a algún centro social, biblioteca, familiar o amigo.**

<alert type="danger">De todas maneras, introducir tus identificadores y contraseña en un ordenador de un tercero significa que confías en ese tercero y también en el hecho de que es un ordenador confiable. Si tienes dudas, nunca introduzcas tus contraseñas en dispositivos ajenos.</alert>
