---
title: ¿El hecho de utilizar la moneda libre me hará libre?. 
description: No, pero te ayudará
---

La moneda libre es compatible con las cuatro libertades económicas fundamentales definidas en el <lexique>TRM</lexique>, pero no garantiza el cumplimiento de los mismos. Si una de las cuatro libertades por lo que sea no se cumple, incluso por un factor que no sea el dinero, entonces este último ya no es libre: 

  1.  **Libertad de elección**: si una moneda es impuesta o se prohíbe, se pierde automáticamente esa libertad. Por ejemplo, una moneda libre impuesta por un gobierno ya no es una moneda libre. 
  2.  **Libertad de uso**: si se impide el acceso a los recursos, por ejemplo, por parte de un capitalista o de un gobierno, sea moneda libre o no, se quita esta libertad. 
  3.  **Libertad para producir**: Si los precios de venta están impuestos por leyes, entonces uno no puede decidir el valor de lo que produce. Si los medios de producción están monopolizados por un grupo restringido, entonces no se puede producir libremente. 
  4.  **Libertad de comercio**: si los intercambios llevan impuestos (tasas), entonces no se puede comerciar libremente. 

Por lo tanto, un gobierno que imponga una moneda libre, que además introduzca impuestos forzados en ésta, haría que esta moneda no fuera libre, al suprimir las libertades de su definición. 

Si se quieren garantizar estas libertades, las construcciones adicionales a la moneda libre son útiles.