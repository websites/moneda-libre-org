---
title: ¿Cuáles son las diferencias de la G1 con los SEL (Sistemas de Intercambios Locales)? 
description: Vinculo e intercambios sin límites.
---
Las SEL utilizan - como moneda de cambio - unidades monetarias que corresponden al tiempo. Por lo general, un minuto para una unidad. 
Estas unidades monetarias (minutos) se crean y destruyen durante los intercambios por débitos y créditos mutuos, sin un control.

*Es imposible saber cuántas unidades hay en circulación.
Hacer trampas es bastante fácil, por lo que requerimos de una gran confianza.*  

Esta falta de control de la monedas en una SEL suelen compensarse estableciendo unas restricciones: 
* débitos y créditos máximos para una cuenta, 
* se establecen cantidades máximas de intercambios, 
* no se permiten intercambios de tipo profesional, 
* los intercambios han de estar localizados, 
* imposibilidad  de transmitir una cuenta mediante la herencia.

La moneda libre permite superar todas estas limitaciones. Porque el dinero se crea de manera controlada mediante la fórmula matemática **DU<sub>(t+1)</sub>=DU<sub>(t)</sub> + c<sup>2</sup> × (M/N)<sub>(t)</sub>**

Una SEL puede optar por utilizar moneda libre para sus intercambios, lo que le permitirá superar estas restricciones.