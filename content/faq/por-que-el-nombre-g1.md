---
title: ¿De dónde vienen estos nombresG1, Duniter, Césium?. 
description: Antecedentes...
---

## Ğ1

**Stéphane Laborde:** "Ğ se eligió como símbolo de lo desconocido, de la novedad de la creación de valor económico indecidible , en referencia al gran lógico Kurt Gödel por su teorema de incompletitud establecido en 1931, así como al proyecto GNU lanzado por Richard Stallman en 1983 (fundador del software libre que ha permanecido y sigue siendo desconocido para muchos, a pesar de que es la columna vertebral del 99% de Internet). 
Mas información en https://www.glibre.org/.

El 1 porque es la primera moneda libre.


## Duniter

Es literalmente una máquina de generación de <lexique>DU</lexique>, como un DUnitor.

También hay una alusión a la duna del logo, cuya arena se renueva con el viento y que parece ser la misma sin ser realmente la misma... (precioso, ¿no te parece?)

## Cesium

El DU es una parte de la moneda creada a intervalos regulares, por lo tanto ligada al tiempo, tiempo medido por el átomo de cesio.

[Extracto de Wikipédia](https://fr.wikipedia.org/wiki/Temps_atomique_international) :

El segundo fue definido en 1967 en la 13ª Conferencia General de Pesos y Medidas como la duración de 9.192.631.770 períodos de radiación correspondientes a la transición entre los dos niveles hiperfinos del estado fundamental del átomo de cesio-133.