---
title: ¿Dónde puedo encontrar la licencia G1?. 
description: La licencia se puede encontrar en el Glosario
---
## En esta página

Puedes encontrar sobre la <lexique title="Licencia">licencia</lexique> en el glosario.

## En la página web de Duniter

La licencia también está disponible en la página web :\
<https://git.duniter.org/documents/g1_monetary_license/-/raw/master/g1_monetary_license_es.rst>

## En Cesium
Desde la versión Cesium de ordenador, vete a la pestaña monedas, encontrarás un botón "ver licencia" en la parte superior de la página.