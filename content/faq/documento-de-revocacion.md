---
title: ¿Para qué sirve el fichero de revocación?. 
description: "Es indispensable en caso de pérdida/robo de los identificadores o clave de la cuenta de un miembro cocreador."
---
## Sin procedimiento de "¿has olvidado tu contraseña?"
En la moneda libre Ğ1, tu cuenta no está administrada por un organismo centralizador, es decir, cada uno es responsable de administrar su cuenta. 
Nadie puede recuperar su nombre de usuario y contraseña. 

<alert type="danger">Este documento solo se puede descargar en un ordenador, ya que la versión de cesium para teléfonos inteligentes tiene errores. </alert>

## En caso de pérdida de identificadores o contraseñas.

Si ya no puedes acceder a tu cuenta, esas junas se pierden y además la cuenta sigue generando moneda para nada. 
Lo mismo ocurre si tu cuenta de co-creador es jaqueada; es una pena continuar creando moneda para el pirata informático.

No puedes hacer que otra cuenta sea cocreadora de moneda, mientras tu cuenta inicial continúe creando moneda. 
La única posibilidad en caso de pérdida o robo del identificador es revocar la cuenta, utilizando el *fichero de revocación*, que es importante tenerlo disponible incluso si se destruye tu ordenador. 

Debe guardarse en diferentes medios: llave USB, nube, etc. 

<alert type="warning">Este documento se basa en la fecha en que solicitó convertirse en co-creador (convertir una cuenta en una cuenta de miembro). 
Por lo que, si tuvieras que volver a iniciar el trámite, deberás descargar de nuevo el documento de revocación y no olvides de borrar el antiguo, que se ha vuelto inservible.</alert>


## Uso

Ver [Cómo revocar mi cuenta](/faq/como-puedo-revocar-mi-cuenta)
