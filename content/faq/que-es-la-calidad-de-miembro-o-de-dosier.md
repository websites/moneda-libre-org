---
title: "¿Qué es la calidad de un miembro y de dosier?"
description: "Sirve para la verificación de la regla de la distancia"
---
## Notación

En función de las herramientas o software utilizados, la calidad aparece como un número entre 0 y 1.25, o bien bajo forma de un porcentaje entre 0 y 100.

- 80 % = 1 
- 100 % = 1.25

Esta calidad es el resultado del cálculo de la <lexique>regla de la distancia</lexique>, en la <lexique title="RdC">red de confianza</lexique>


## Calidad de dosier

Para un futuro miembro entrante la calidad de dosier corresponde al número de miembros <lexique title="referente">referentes o tejedores</lexique>, de los cuales la persona se encuentra a menos de **5 pasos**.

Para convertirse en miembro, se necesita una calidad de dosier >= 1 (>=80%).
Esta calidad se recalcula y verifica cuando la membresía ha de renovarse (cada año).

Una cuenta candidata que no tenga esta calidad de dosier suficiente, no podrá convertirse en miembro o si ya lo es, perderá su membresía en el momento de la renovación. Esto puede pasar si uno de los certificadores ha perdido certificaciones importantes (de otros cocreadores activos que les acerquen significativamente al resto de la red).

Esta calidad de dosier se llama *distancia* en la utilidad [wotwizard] (https://wot-wizard.duniter.org/20distances).

Se puede comprobar la calidad de dosier de una candidatura a miembro mediante la utilidad [g1-monnit](https://monit.g1.nordstrom.duniter.org/willMembers?lg=en)

Estas informaciones también se encuentran de forma más visual en [wotwizard-ui](https://wotwizard.axiom-team.fr/)


## Calidad de enlace de miembro

Esta calidad corresponde al % de número de miembros referentes a los cuales dicho miembro se encuentra a una distancia máxima de **4** pasos. Es útil para saber si la certificación de dicho miembro puede ayudar significativamente a cumplir la regla de la distancia (que es de 5 pasos max.) a un candidato y por lo tanto

Alguien certificado por un miembro con una calidad superior o igual a 1 (80%) tendrá una calidad de dosier suficiente (de 1 o 80%). No obstante, esto no es indispensable. Un candidato puede obtener una calidad de dosier >=1 teniendo a certificadores de calidad menor, pero de regiones diferentes de la red de confianza, que no sean de la misma red del grupo local.