---
title: ¿Una transacción puede ser anónima?
description: Hay utilidades que lo permiten.
---

## El protocolo

El protocolo actual todavía no permite hacer directamente una transacción anónima: la lista de las transacciones enviadas o recibidas por una cuenta es pública en la <lexique>blockchain</lexique>, y el emisor y receptor de cada transacción son públicos, como en la mayoría de criptomonedas.

Como la G1 no obliga a introducir ningún dato personal, ni teléfono ni email, ni nombre (una cuenta se crea solo pensando en unas contraseñas), nadie tiene por qué saber quién está detrás de cada llave pública si alguien quiere guardar su anonimato. 

Las cuentas miembro que son cocreadores del <lexique>DU</lexique>, sólo necesitan un pseudónimo (o <i>nickname</i>) y que te conozcan bien (privadamente) mínimo otros 5 cocreadores. Estos cocreadores no tienen por que revelar a nadie quien está detrás de esa cuenta.

La trazabilidad de las transacciones es necesaria para la seguridad de la moneda, de un punto de vista técnico. Sin embargo, se puede hacer una transacción 100% intrazable al origen mediante la técnica del "mezclado" (frecuentemente usada con otras criptos): varias personas envían una transacción a una cuenta intermediaria (cuenta mezcladora), y ésta automáticamente vuelve a enviar estas transacciones a las cuentas destino. Ya no se puede saber quién ha enviado a quién.

Un software que permite "mezclado" fácilmente y de forma segura se puede encontrar <b>[aquí](https://zettascript.org/projects/gmixer/)</b>


## Consejos

Para tener un uso 100% anónimo de la G1, puedes crear tantos monederos simples como quieras y no poner ningún identificador o dato personal, ni añadir comentarios en las transacciones. Puedes usar unos monederos para recibir pagos y otros para hacer los pagos, y usar <i>mezcladores</i> para transferir saldo de unos a otros si es necesario.