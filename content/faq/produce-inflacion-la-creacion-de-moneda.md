---
title: ¿La creación de moneda produce inflación? 
description: ¿El poder de adquisión decrece con la creación monetaría?
---
## ¿La creación de dinero me hace perder poder adquisitivo?

En la mente de la mayoría de la gente está esta idea:

**creación monetaria = imprimir dinero = inflación**

El término "inflación" se refiere al aumento general en el nivel de precios. 

En los sistemas monetarios más difundidos (dinero deuda), este aumento general del nivel de precios se atribuye generalmente al aumento de la oferta monetaria. 

En la moneda libre, la oferta monetaria crece a un ritmo predecible: 10% anual. ¿Significa esto que los precios de todos los productos básicos aumentan un 10% cada año? Si ..... y no. Para entender esto, ubiquémonos en un sistema que ya conoces: el sistema Euro.

#### El ejemplo de la pizza

Imagínate que en 2019 podrías crear 150 €/mes, es decir, 5 €/día. 

Llamemos a esos 5€ que creas todos los días, el Dividendo Universal (o DU). 

Todos los jueves por la noche sales con tus amigos, cenáis cada uno una pizza y pagáis 10 €.

En comparación con la moneda que creas todos los días, una pizza representa : 

**10 / 5 = 2 UD**

¿Qué pasará en 2020? 

La oferta monetaria aumentará un 10 %.

Tu pizzero favorito puede repercutir este aumento de la masa monetaria sobre sus precios, 
con lo cual pagarías 11€/pizza, frente a los 10 € que pagabas antes. 

**Sí, pero ojo :** 

¡eres tú quien será responsable de este aumento en la masa monetaria!,
porque en lugar de crear 5€ cada día, ahora crearás 5,50€. 

Así que, la pizza - en comparación con la moneda que creas todos los días - pizza representará :

**11 / 5,5 = 2 UD**

¿Habrás perdido poder adquisitivo?

**Al final es una astucia para que los precios no se muevan.**

Además, no te preocupes por esto:

Los "actores" económicos seguramente optarán por fijar los precios en DU y, como acabas de ver con el ejemplo de la pizza, si aumenta la masa monetaria no cambia el precio en DU.

## ¿La creación monetaria debilita mis ahorros?

Esto puede ser cierto en algunos casos especiales: 

Para aquellos cuyo saldo es superior al saldo promedio por miembro, 
si los precios disminuyen en DU, el aumento en el número de G1 
en una DU disminuirá el número de DU en propiedad. 

Para aquellos cuyo saldo se encuentre por debajo del saldo promedio por miembro, el pago de la DU incrementará el número de DU en propiedad.

Con el tiempo, los saldos de cada uno convergen hacia el saldo promedio.

Esta es una de las grandes propiedades de la moneda libre: si dos individuos cuya masa monetaria está lejos de la media, se ve que sus porcentajes de masa monetaría convengerían si no tuvieran actividad económica en 40 años. [![](https://monnaie-libre.fr/wp-content/uploads/2019/02/convergence-des-soldes.png)][0]

Sin embargo, no hay que olvidarse que los valores económicos varían con el tiempo, es decir, por regla general, el valor de los bienes producidos aumenta con el tiempo. 

Por ejemplo, aunque un ordenador portátil en 2019 es muy superior (en cuanto a prestaciones) a uno de sobremesa del 2009, es mucho más barato, incluso si lo contamos en términos absolutos (euros), y aunque la masa monetaria en euros ha aumentado mucho en este intervalo de 10 años.

[0]: https://monnaie-libre.fr/wp-content/uploads/2019/02/convergence-des-soldes.png