---
title: ¿Cómo puedo borrar una cuenta?. 
description: No se puede; solo se puede borrar el perfil de la cuenta.
---
## No puedes borrar una cuenta

Desde el momento que una cuenta recibe una transacción, aparece en la cadena de bloques y permanecerá allí para siempre. 
*Esta cuenta no se puede cerrar ni cancelar.*

## Cuenta de cocreador (miembro)

Una cuenta de miembro puede ser revocada. Esto no quiere decir que se elimine la cuenta. La revocación solo transforma la cuenta del cocreador en una cuenta simple, es decir, ya no creará el DUğ1, ni podrá emitir la certificación. 

Ver [Cómo revocar mi cuenta](/faq/como-puedo-revocar-mi-cuenta)


## Cuenta simple (monedero)

Solo se puede eliminar el perfil que está asociado a la cuenta. En Cesium en la pestaña "mi cuenta", pinchas en "editar mi perfil", en "opciones" y luego en "eliminar mi perfil".
 
