---
title: Cuál es el tipo de cambio Ğ1/€?. 
description: "Para la Ğ1, no hay un tipo de cambio."
---

# ¿Cuánto vale 1 Ğ1 en euros?

Difícil de decir.

Para empezar: la Ğ1 no es la unidad más conveniente para comparar el valor de la moneda libre frente a otras.

De hecho, debido a que el número de junas aumenta constantemente, el valor de un Ğ1 probablemente tenderá a disminuir en comparación con todos los demás valores económicos.

Por lo tanto, es mejor interesarnos por el valor de un DU.

# ¿Cuánto vale 1 DU en euros?

Todavía se hace muy complicado de valorar.

Hay varias formas para poder hacerse una idea:

- En ğchange: activa los anuncios cerrados haciendo clic en "búsqueda avanzada" y escribe en la barra de búsqueda las palabras clave "euros" o "UNL". Algunos anuncios que encontrarás en esta página de resultados, corresponderán a ventas de euros a cambio de Ğ1 (o compras de euros a cambio Ğ1).
- En el foro francés: encontrarás temas como este: “Puja: 50 UNL”, en el que el autor especifica cuántos Ğ1 pudo obtener vendiendo euros en  la subasta.
- Comparando el precio de los bienes vendidos en ğchange con el mismo tipo de bienes vendidos en plataformas de venta en euros, como Le Bon Coin, por ejemplo.

# A modo de Ejemplo 

Un  FairPhone 2 nuevo se vendió por 13.000 Ğ1 en una subasta el 16 de septiembre de 2018 y otro por 12.048 Ğ1 el 23 de diciembre de 2018. Su precio en euros ronda los 399,00 €. 

Cuando se trata de comercio de divisas, aquí hay algunos datos históricos:

| Fecha             | Contexto                              | Vendedor en €                                   | Vendedor de Ğ1     | Tasa (Ğ1/€) |
| ----------------  | ------------------------------------- | --------------------------------------------- | -------------------- | ----------- |
| 9 octubre 2017    | anuncio ğchange                       | cgeek                                         | N.C                  | 150         |
| 23 marzo 2018     | anuncio ğchange                       | Thierry                                       | Matiou,Lesipermanpot | 10 à 30     |
| 28 diciembre 2018 | anuncio ğchange                       | personne                                      | Gabriel              | 25          |
| 1er marzo 2019    | Subasta en el foro   monnaie-libre.fr | Looarn                                        | LucAstarius          | 30          |
| 10 marzo 2019     | finales de Festi’Junio 2 Mayenne      | de nombreuses et nombreux ğunistes            | Le Sou               | 50          |
| continu           | magazine, ğmercados                   | Djoliba (tienda de música cerca de Toulouse) | les clients          | 5 puis 10   |
|Todos los meses en 2021|50€ cada mes sur Gchange|Cukoland||≈20|