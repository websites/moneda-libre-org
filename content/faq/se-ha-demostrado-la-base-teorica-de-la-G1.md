---
title: ¿Se ha demostrado la base teórica de la Ğ1?
description: "La Ğ1 es un experimento"
---

La moneda libre Ğ1 se basa en la Teoría Relativa del Dinero (<lexique>TRM</lexique>), demostrando la posibilidad de disponer de moneda libre tal como la define. 

Para comprobar su validez y las herramientas matemáticas que proporciona, se realizaron varios ensayos: 

* Ğeconomicus es un juego de simulación de economía. Sus resultados muestran que la moneda libre es más efectiva que el dinero deuda (€) para resolver desigualdades, fomentando la producción de valores y los intercambios. 
* El Ğ1 es la primera aplicación de moneda libre de escala media. Incluso si la muestra aún es demasiado pequeña, para sacar conclusiones sobre las desigualdades, vemos que funciona.