---
title: "¿Qué pasa con la moneda de los fallecidos?"
description: "Se hace insignificante a lo largo del tiempo"
---
Si la persona que ha fallecido no se ha ocupado de dejar sus códigos de acceso a sus herederos, la moneda permanece en su cuenta. 
La membresía no se podrá renovar y al final de esta (menos de 1 año), la cuenta ya no generará moneda.
Si los herederos tienen acceso a la cuenta pueden recuperar la moneda por transferencia. Para ello, tendrán que revocar la cuenta del fallecido para respetar la licencia.

Gracias al dividendo universal, la cantidad de dinero sigue creciendo exponencialmente cuantitativamente.
La moneda creada por personas fallecidas representa una parte cada vez más pequeña del dinero existente, hasta que se vuelve insignificante después de unos 40 años.
    
![La curva sube cada vez más lentamente, y baja tan pronto como deja de haber creación monetaria](https://duniter.github.io/prez_rmll2017/images/evolution_compte.png "Evolución de una cuenta miembro en DU anual")
 
La imagen representa la parte de la creación monetaria, en relación con la masa total monetaria para una persona que murió a los 80 años.
