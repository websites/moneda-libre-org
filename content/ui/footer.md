La moneda libre la lleva una comunidad de seres humanos, no depende de ninguna organización "oficial".
Existen muchas [otras páginas](/ressources?filters=Web) y [colectivos](/ressources?filters=asociacion&filters=grupo-local) sobre la moneda libre para aprender y dialogar…
Aunque esta página está en español, pretende ser comprensible y útil para la mayor parte de la gente.
Puedes conocer a los colaboradores de esta página durante los eventos de moneda libre y debatir con ellos en el foro para [contribuir](/contribuir) tú también.