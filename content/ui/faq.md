---
title: Preguntas y Respuestas (FAQ)
---
## Grupos locales para poner las preguntas en directo
No dudes en acercarte a tu grupo local más cercano para debatir y experimentar sobre la moneda libre con gente que ya están utilizando la Ğ1.
### [Ver el mapa de mercados y grupos locales](/#map)

Existe una plataforma online de debate para tus preguntas y dudas conocida como el [foro](https://foro.moneda-libre.org).

Consejo: utiliza la función de [búsqueda del foro](https://foro.moneda-libre.org/search) antes de quizás repetir una pregunta recurrente.