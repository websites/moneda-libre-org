---
title: Chemin de fer
description: Parcours de lecture. Affiché sous le héro de la page d'accueil, et dans les pages concernées.
layout: cdc
---

# [1. Descubrir](/descubrir)

- ¿ De dónde viene el dinero ?
- La creación monetaria con moneda libre

---

# [2. Comprender](/comprender)

- La teoría relativa de la moneda
- La red de confianza y la blockchain de la Ğ1

---

# [3. Comenzar](/comenzar)

  - Abrir una cuenta
  - Las herramientas de la Ğ1

---

# [4. Contribuir](/contribuir)

- Encontrar a la comunidad
- Participar
