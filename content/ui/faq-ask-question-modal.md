---
title: Escribe tu pregunta
inputTitle: null
inputPrecision: null
---

¿ No has encontrado respuestas a tu pregunta en la sección Preguntas Y Respuestas (FAQ) ?
¡ Proponlo <a href="https://foro.moneda-libre.org/c/sugerencias-sobre-el-foro-web/2" target="_blank"><b>en este enlace</b></a>!

<!-- <t-input-group label="Tu pregunta" description="Título de la pregunta tal cual te gustaría si la recibieras tú." class="mt-3">
    <t-input v-model="inputTitle" placeholder="Título de la pregunta"></t-input>
</t-input-group>

<t-input-group label="Complemento" description="Indica aquí más detalles sobre la pregunta, una respuesta posible, recursos, enlaces, etc." class="my-3">
    <t-textarea v-model="inputPrecision" placeholder="Informaciones complementarias..."></t-textarea>
</t-input-group> -->
- - -
<br>
Gracias por tu aporte !
Nos esforzaremos para responder lo más rápido posible...
