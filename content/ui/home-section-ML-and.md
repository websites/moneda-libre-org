---
title: Section monnaie libre et...
dev-notes: It's look ugly for now, but it will be better and easier to build block in next version with vuejs component in markdown...
---

<nuxt-link to="/la-moneda-libre-y-el-medio-ambiente" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8">
      <img src="/img/healthy-eating.svg" class="w-4/5 md:w-2/5" />
      <div>
        <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">El medio ambiente</h2>
        <p class="text-base lg:text-lg">La Ğ1 no utiliza una potencia de cálculo para asegurar la cadena de bloques. La creación monetaria mediante el DU permite que ya no haya que pasar por un endeudamiento perpetuo para crear la moneda. Producir a toda costa ya no es un fin en sí mismo. Además, el DU es un primer paso hacia un estilo de vida decrecentista.</p>
        <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">Saber más…</div>
      </div>
  </section>
</nuxt-link>

<nuxt-link to="/la-moneda-libre-y-la-democracia" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8 flex-col-reverse">
    <div>
      <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">La democracia</h2>
      <p class="text-base lg:text-lg"><i>Dadme el control de la moneda de un país y no me importará quién hace las leyes (Mayer Amschel Rothschild)</i>. El privilegio de crear dinero es un poder. Repartir la creación entre toda la población es mucho más democrático.</p>
      <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">Saber más…</div>
    </div>
     <img src="/img/work-life-balance.svg" class="w-3/5 md:w-1/4" />
  </section>
</nuxt-link>

<nuxt-link to="/la-moneda-libre-y-las-cripto-monedas" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
<section class="flex flex-col md:flex-row items-center gap-8">
    <img src="/img/High-quality-products.svg" class="w-3/5 md:w-1/3" />
    <div>
      <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">Las criptomonedas</h2>
      <p class="text-base lg:text-lg">La Ğ1 es técnicamente una criptomoneda, pero su modelo de creación monetaria la convierte en una moneda mucho más justa y ecológica. Los primeros en entrar no tienen privilegio o ventaja sobre los nuevos.</p>
      <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">Saber más…</div>
    </div>
  </section>
</nuxt-link>

<nuxt-link to="/la-moneda-libre-y-las-monedas-locales" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8 flex-col-reverse">
    <div>
      <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">Las monedas locales</h2>
      <p class="text-base lg:text-lg">Por lo general, las monedas locales están indexadas al euro o moneda fiat de cada país. No es un cambio de paradigma. Eres pobre en euros, eres pobre en moneda local.</p>
      <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">Saber más…</div>
    </div>
    <img src="/img/MLC.svg" class="w-3/5 md:w-1/3" />
  </section>
</nuxt-link>
