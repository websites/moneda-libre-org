---
description: Texte d'introduction en page d'accueil sous la section de recherche
---

## Otra nueva moneda

Una moneda es un instrumento de intercambio, medición y depósito de valor.\
Cualquier cosa puede ser una moneda, es el uso lo que hace que un objeto sea una moneda. Los cereales (ej. el trigo), la sal (de donde viene la palabra "salario") se han utilizado durante mucho tiempo como moneda. Luego llegaron las monedas fiduciarias (basadas únicamente en la confianza).

**Al utilizar el dinero deuda, confías en los banqueros.**

Debemos preguntarnos cómo se genera el dinero. ¿De dónde viene? ¿Cómo se crea? ¿Por quién?

## Una moneda diferente

La moneda libre sólo se crea en los monederos de los seres humanos vivos.\
La creación de moneda libre se distribuye por igual entre todos los miembros de la comunidad, en el espacio y en el tiempo.\
Es decir, no importa dónde o cuándo vivan, cada miembro crea la misma porción de moneda, a lo largo de su vida.

La misma porción no significa la misma cantidad, cuando la cantidad de moneda en circulación crece, la porción creada por cada miembro también crece.

## Una moneda relativa

Hay que ser consciente de que lo que cuenta no es la cantidad de moneda que se posee, sino la parte relativa de moneda en relación con la masa monetaria global.\
En la moneda libre, ya no se cuenta en cantidad, sino en términos relativos. Usamos una unidad relativa de moneda, el <lexique>DU</lexique> cotidiano.
