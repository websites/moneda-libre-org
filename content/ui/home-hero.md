<section class="xl:mr-20">
  <h1 class="mb-4 text-4xl md:text-5xl xl:text-6xl bg-clip-text text-transparent bg-gradient-to-r from-purple-800 to-blue-600 font-extrabold leading-tight slide-in-bottom-h1">
  La moneda libre
  </h1>

  <p class="leading-normal text-xl text-gray-700 dark:text-gray-300 md:text-2xl xl:text-4xl mb-12 font-semibold slide-in-bottom-subtitle">
  Repensando la creación del dinero…<br />¡ y experimentándola !
  </p>

  <p class="leading-normal text-base text-gray-700 dark:text-gray-300 xl:text-xl mb-6 slide-in-bottom-subtitle">
  Un modelo económico más justo y sostenible es posible.<br />Una moneda libre situa al ser humano en el corazón de la economía y toma en cuenta a las generaciones futuras.
  </p>

  <p class="leading-normal text-base text-gray-700 dark:text-gray-300 xl:text-xl mb-6 slide-in-bottom-subtitle">
  La Ğ1 (la "Juna") es la primera moneda libre. Concebida sobre una cadena de bloques ("blockchain") ecológica, es una experiencia comunitaria, solidaria... ¡ y quizás subversiva !
  </p>
</section>
