export default {
  cancel: 'Cancelar',
  search: 'Buscar',
  goBack: 'Volver',

  noResult: {
    text: 'No se encontraron resultados…',
    searchWholeSite: 'Buscar "{query}" en todas la web',
    searchOnForum: 'Buscar "{query}" en el foro',
  },
  lexique: {
    title: 'Léxico',
    searchPlaceholder: 'Buscar un término en el léxico…',
    tooltipReadmore: 'Leer más…',
  },
  faq: {
    searchPlaceholder: 'Buscar en preguntas y respuestas…',
    submitQuestion: 'Enviar pregunta',
    blockSubmitQuestionTitle: '¿Aún tienes dudas?',
    blockSubmitQuestionButton: '¡Pregúntanos!',
  },
  home: {
    title: 'Inicio',
    searchSitePlaceholder: '…en esta web, blog, FAQ, léxico…',
    searchRessourcesPlaceholder: '…una web, una app, un vídeo…',
    searchG1Placeholder: '…un monedero, una llave pública…',
  },
  page: {
    updatedAt: 'Actualizada el',
    editAdmin: 'Editar esta página',
    editVscode: 'Editar con editor de código',
    toc: 'Índice de contenido',
    related: 'Ver también',
  },
  recherche: {
    title: 'Buscar',
    searchPlaceholder: 'Buscar en la web…',
    forumResultTitle: 'Resultados en el foro',
    noForumResult: '¡ … y nada en el foro !',
  },
  blog: {
    title: 'Blog',
    searchPlaceholder: 'buscar una entrada del blog…',
  },
  ressources: {
    title: 'Recursos',
    searchPlaceholder: 'Buscar un recurso…',
    submitResources: 'Proponer un recurso',
  },
}
