import { execSync } from 'child_process'
import i18n from './plugins/i18n.js'
import config from './static/settings/globals.json'
import hooks from './libs/hooks.js'
import icons from './libs/icons.js'
import sitemapRoutes from './libs/sitemapRoutes.js'
import feedFactory from './libs/feedFactory.js'

const publicRuntimeConfig = {
  site_url: process.env.site_url || 'https://moneda-libre.org',
  forum_url: process.env.forum_url || 'https://foro.moneda-libre.org',
  map_url: process.env.map_url || 'https://carte.monnaie-libre.fr',
  twitter_user: process.env.twitter_user || 'Moneda_Libre',
  fediverse_server: process.env.fediverse_server || 'chaos.social',
  fediverse_user: process.env.fediverse_user || 'g1monedalibre',
  telegram_group: process.env.telegram_group || 'g1monedalibre',
  peertube_url: process.env.peertube_url || 'https://tube.p2p.legal/my-library/video-playlists/6e6ac81e-888d-4101-9d2b-e5e55c813649',
  social_networks_hashtags:
    process.env.social_networks_hashtags || 'MonedaLibre,Ğ1,junas,economía',
  gitlab_repo_path: process.env.gitlab_repo_path || 'websites/moneda-libre-org',
  git_commit: execSync('git log --pretty=format:"%h" -n 1').toString().trim(),
  ...config,
}

export default {
  // Deployment target
  target: 'static',

  // Add 404.html page
  generate: {
    fallback: true,
  },

  // Config
  publicRuntimeConfig,

  /**
   * Global page headers (https://go.nuxtjs.dev/config-head)
   */
  head: {
    title: config.site_title || process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          config.site_description || process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' }],
    bodyAttrs: {
      class: process.env.NODE_ENV === 'development' ? 'debug-screens' : '',
    },
  },

  /**
   * Customize the progress-bar color
   */
  loading: { color: '#7c3aed' },

  /**
   * Global CSS (https://go.nuxtjs.dev/config-css)
   */
  css: [],

  /**
   * Auto import components (https://go.nuxtjs.dev/config-components)
   */
  components: ['~/components', '~/components/app/'],

  /**
   * Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
   */
  plugins: ['~plugins/vue-tailwind', '~/plugins/directives.client.js'],

  /**
   * Modules for dev and build (https://go.nuxtjs.dev/config-modules)
   */
  buildModules: [
    // https://composition-api.nuxtjs.org/
    // TODO: remove it when nuxt3 released
    '@nuxtjs/composition-api',
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://github.com/nuxt-community/svg-module
    '@nuxtjs/svg',
    // https://color-mode.nuxtjs.org/
    '@nuxtjs/color-mode',
    // https://github.com/nuxt-community/fontawesome-module
    [
      '@nuxtjs/fontawesome',
      {
        component: 'fa', // component name
        addCss: false,
        icons,
      },
    ],
  ],

  /**
   * Nuxt.js modules (https://go.nuxtjs.dev/config-modules)
   */
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://pwa.nuxtjs.org
    '@nuxtjs/pwa',
    // https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    // https://github.com/dword-design/nuxt-content-body-html
    'nuxt-content-body-html',
    // https://content.nuxtjs.org/fr
    '@nuxt/content',
    // https://i18n.nuxtjs.org
    ['nuxt-i18n', i18n],
    // https://github.com/Chantouch/nuxt-clipboard
    'nuxt-clipboard',
    // https://github.com/nicolasbeauvais/vue-social-sharing
    'vue-social-sharing/nuxt',
    // https://github.com/nuxt-community/feed-module
    '@nuxtjs/feed',
    // https://sitemap.nuxtjs.org/fr/
    '@nuxtjs/sitemap',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // https://pwa.nuxtjs.org/manifest
  pwa: {
    meta: {
      name: 'Moneda Libre',
      author: null,
      lang: 'es',
      theme_color: '#5b21b6',
      description:
        config.site_description || process.env.npm_package_description,
      ogHost: 'monnaie-libre.fr',
      twitterCard: 'summary_large_image',
      twitterSite: '@monnaie_libre',
      twitterCreator: '@monnaie_libre',
    },
    manifest: {
      name: 'Moneda Libre',
      short_name: 'moneda-libre.org',
      lang: 'es',
    },
    icon: {
      fileName: 'icon-app.png',
    },
  },

  // https://content.nuxtjs.org/fr/configuration
  content: {
    markdown: {
      remarkPlugins: ['remark-breaks'],
    },
  },

  // https://color-mode.nuxtjs.org
  colorMode: {
    preference: 'system', // default value of $colorMode.preference
    fallback: 'light', // fallback value if not system preference found
    classSuffix: '',
  },

  // https://github.com/Chantouch/nuxt-clipboard
  clipboard: {
    autoSetContainer: true,
  },

  /**
   * Nuxt hooks
   * https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-hooks
   */
  hooks,

  /**
   * Nuxt sitemap
   * https://sitemap.nuxtjs.org/fr/guide/configuration
   */
  sitemap: {
    hostname: publicRuntimeConfig.site_url,
    gzip: true,
    routes: sitemapRoutes,
  },

  /**
   * Generate feed
   * https://github.com/nuxt-community/feed-module
   */
  feed: {
    data: {
      publicRuntimeConfig,
      config,
    },
    factory: feedFactory,
  },

  /**
   * Build configuration
   */
  build: {
    extractCSS: true,
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {

      // config.module.rules.push({
      //   enforce: 'pre',
      //   test: /\.(js|vue)$/,
      //   loader: 'eslint-webpack-plugin',
      //   exclude: /(node_modules)/,
      //   options: {
      //     fix: true
      //   }
      // })
    },
  },
}
